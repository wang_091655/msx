package com.msx.trade.service;

import java.math.BigDecimal;


public interface Disanfangservice {
    public String identityAuth(String name, String cardId, String accNo, String bankName, String bankShortName, String merTradeNo, BigDecimal amount);
    public String fangkuanrenzheng(String thirdTradeNo);

}
