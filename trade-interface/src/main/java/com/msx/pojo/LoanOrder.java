package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "tb_loan_order")
public class LoanOrder implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private String orderId;

    /**
     * 第三方交易订单号
     */
    @Column(name = "thirdTradeNo")
    private String thirdtradeno;

    /**
     * 第三方交易状态 0:处理中1:交易成功2:交易失败
     */
    @Column(name = "resCode")
    private String rescode;

    /**
     * 错误编码 E200交易成功,E201渠道不支持，E202卡号错误
     */
    @Column(name = "msgCode")
    private String msgcode;

    /**
     * 错误具体信息
     */
    @Column(name = "msgInfo")
    private String msginfo;

    /**
     * 姓名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 身份证号
     */
    @Column(name = "identity_code")
    private String identityCode;

    /**
     * 银行卡号
     */
    @Column(name = "bank_code")
    private String bankCode;

    /**
     * 银行名称
     */
    @Column(name = "bank_name")
    private String bankName;

    /**
     * 银行简称
     */
    @Column(name = "bank_short")
    private String bankShort;

    /**
     * 放款时间
     */
    @Column(name = "loan_time")
    private Date loanTime;

    /**
     * 放款金额
     */
    @Column(name = "loan_money")
    private BigDecimal loanMoney;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 获取第三方交易订单号
     *
     * @return thirdTradeNo - 第三方交易订单号
     */
    public String getThirdtradeno() {
        return thirdtradeno;
    }

    /**
     * 设置第三方交易订单号
     *
     * @param thirdtradeno 第三方交易订单号
     */
    public void setThirdtradeno(String thirdtradeno) {
        this.thirdtradeno = thirdtradeno == null ? null : thirdtradeno.trim();
    }

    /**
     * 获取第三方交易状态 0:处理中1:交易成功2:交易失败
     *
     * @return resCode - 第三方交易状态 0:处理中1:交易成功2:交易失败
     */
    public String getRescode() {
        return rescode;
    }

    /**
     * 设置第三方交易状态 0:处理中1:交易成功2:交易失败
     *
     * @param rescode 第三方交易状态 0:处理中1:交易成功2:交易失败
     */
    public void setRescode(String rescode) {
        this.rescode = rescode == null ? null : rescode.trim();
    }

    /**
     * 获取错误编码 E200交易成功,E201渠道不支持，E202卡号错误
     *
     * @return msgCode - 错误编码 E200交易成功,E201渠道不支持，E202卡号错误
     */
    public String getMsgcode() {
        return msgcode;
    }

    /**
     * 设置错误编码 E200交易成功,E201渠道不支持，E202卡号错误
     *
     * @param msgcode 错误编码 E200交易成功,E201渠道不支持，E202卡号错误
     */
    public void setMsgcode(String msgcode) {
        this.msgcode = msgcode == null ? null : msgcode.trim();
    }

    /**
     * 获取错误具体信息
     *
     * @return msgInfo - 错误具体信息
     */
    public String getMsginfo() {
        return msginfo;
    }

    /**
     * 设置错误具体信息
     *
     * @param msginfo 错误具体信息
     */
    public void setMsginfo(String msginfo) {
        this.msginfo = msginfo == null ? null : msginfo.trim();
    }

    /**
     * 获取姓名
     *
     * @return user_name - 姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置姓名
     *
     * @param userName 姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取身份证号
     *
     * @return identity_code - 身份证号
     */
    public String getIdentityCode() {
        return identityCode;
    }

    /**
     * 设置身份证号
     *
     * @param identityCode 身份证号
     */
    public void setIdentityCode(String identityCode) {
        this.identityCode = identityCode == null ? null : identityCode.trim();
    }

    /**
     * 获取银行卡号
     *
     * @return bank_code - 银行卡号
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * 设置银行卡号
     *
     * @param bankCode 银行卡号
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode == null ? null : bankCode.trim();
    }

    /**
     * 获取银行名称
     *
     * @return bank_name - 银行名称
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * 设置银行名称
     *
     * @param bankName 银行名称
     */
    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    /**
     * 获取银行简称
     *
     * @return bank_short - 银行简称
     */
    public String getBankShort() {
        return bankShort;
    }

    /**
     * 设置银行简称
     *
     * @param bankShort 银行简称
     */
    public void setBankShort(String bankShort) {
        this.bankShort = bankShort == null ? null : bankShort.trim();
    }

    /**
     * 获取放款时间
     *
     * @return loan_time - 放款时间
     */
    public Date getLoanTime() {
        return loanTime;
    }

    /**
     * 设置放款时间
     *
     * @param loanTime 放款时间
     */
    public void setLoanTime(Date loanTime) {
        this.loanTime = loanTime;
    }

    /**
     * 获取放款金额
     *
     * @return loan_money - 放款金额
     */
    public BigDecimal getLoanMoney() {
        return loanMoney;
    }

    /**
     * 设置放款金额
     *
     * @param loanMoney 放款金额
     */
    public void setLoanMoney(BigDecimal loanMoney) {
        this.loanMoney = loanMoney;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}