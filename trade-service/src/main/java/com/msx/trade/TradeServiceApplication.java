package com.msx.trade;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.msx.credit.service.Orderservice;
import com.msx.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author msx
 * @create: 2020-06-13 16:45
 */
@SpringBootApplication
@EnableScheduling
@EnableDubbo
@MapperScan("com.msx.trade.mapper")
public class TradeServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(TradeServiceApplication.class,args);
    }
   @Bean
    public IdWorker idWorker(){
        return new IdWorker();
   }


}
