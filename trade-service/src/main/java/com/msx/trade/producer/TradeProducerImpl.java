package com.msx.trade.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.BillOrder;
import com.msx.trade.mapper.UserMapper;
import com.msx.trade.service.Tradeservice;
import com.msx.trade.service.impl.TradeServiceImpl;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-16 10:19
 */
@Service(timeout = 6000,retries=0,version = "2.0.0")
public class TradeProducerImpl implements Tradeservice {

    @Autowired
    private TradeServiceImpl tradeService;



    @Override
    public ResponseResult fangkuan(String state,String id) {

        tradeService.fangkuanpanduan(state,id);



        return new ResponseResult(20000,"成功");
    }
}
