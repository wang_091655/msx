package com.msx.trade.producer;


import com.alibaba.dubbo.config.annotation.Service;
import com.msx.trade.service.Huankuanservice;
import com.msx.trade.service.impl.Huankuanserviceimpl;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

@Service(timeout = 6000,retries=0,version = "2.0.0")
public class HuankuanProducerimpl implements Huankuanservice {


    @Autowired
    private Huankuanserviceimpl huankuanserviceimpl;

    @Override
    public ResponseResult huankuan(String bid) {

        return huankuanserviceimpl.huankuan(bid);
    }

    @Override
    public ResponseResult zhuangdanlb() {



         return huankuanserviceimpl.zhuangdan();
    }

    @Override
    public void hkyzm(HttpServletRequest request) {
        huankuanserviceimpl.hkyzm(request);

    }

    @Override
    public void hkjk(HttpServletRequest request,String yzm,String bid) {
        huankuanserviceimpl.hkjk(request,yzm,bid);
    }
}
