package com.msx.trade.service.impl;


import com.msx.pojo.BillOrder;
import com.msx.pojo.Order;
import com.msx.trade.mapper.BillOrderMapper;
import com.msx.trade.mapper.OrderMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Service
@Log4j2
public class Faxijisuan {
    @Autowired
    private BillOrderMapper billOrderMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Scheduled(cron = "0 0 1 * * ?")
    public void faxijisuan(){
        log.info("*****************************");
        List<BillOrder> billOrders = billOrderMapper.selectAll();
        for(BillOrder billOrder:billOrders){
            String state = billOrder.getState();
            if(!state.equals("1")){
                Date date = new Date();
                Date endTime = billOrder.getEndTime();
                Calendar cal = Calendar.getInstance();
                cal.setTime(endTime);
                long time1 = cal.getTimeInMillis();
                cal.setTime(date);
                long time2 = cal.getTimeInMillis();
                long between_days=(time2-time1)/(1000*3600*24);
                int i = Integer.parseInt(String.valueOf(between_days));

                String orderId = billOrder.getOrderId();
                Order selectorderid = orderMapper.selectorderid(orderId);
                if(i>0){
                    //修改账单
                    BigDecimal faChargee = selectorderid.getFaCharge();
                    BigDecimal whichMoney = billOrder.getWhichMoney();
                    BigDecimal bigDecimal = new BigDecimal(i);

                    BigDecimal bigDecimale2 = null;
                    bigDecimale2 = bigDecimal.multiply(faChargee);
                    BigDecimal bigDecimale3 = null;
                    //计算出罚息总金额
                    bigDecimale3 = bigDecimale2.multiply(whichMoney);
                    //计算出金额
                    BigDecimal bigDecimale4 = null;
                    bigDecimale4 = bigDecimale3.add(whichMoney);
                    billOrder.setWhichMoney(bigDecimale4);
                    billOrder.setState("2");
                    billOrderMapper.updateByPrimaryKeySelective(billOrder);
                    //修改订单
                    selectorderid.setState("6");
                    selectorderid.setOverdue(selectorderid.getOverdue()+i);
                    //修改罚息总金额
                    BigDecimal fine = selectorderid.getFine();

                    bigDecimale3 = bigDecimale2.multiply(whichMoney);
                    BigDecimal add = fine.add(bigDecimale3);
                    selectorderid.setFine(add);
                    //修改应还金额
                    BigDecimal shouldMoney = selectorderid.getShouldMoney();

                    BigDecimal addsm = shouldMoney.add(add);
                    selectorderid.setShouldMoney(addsm);
                    orderMapper.updateByPrimaryKeySelective(selectorderid);


                }
            }
        }

    }
}
