package com.msx.trade.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.msx.pojo.*;
import com.msx.trade.mapper.*;
import com.msx.trade.utils.HttpClientUtils;
import com.msx.trade.utils.MD5Sign;
import com.msx.user.service.IdentityServiceInterface;
import com.msx.user.service.TiedcardServiceInterface;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dutao
 * @create: 2020-06-16 10:39
 */
@Service
public class TradeServiceImpl  {

    private String merId;


    private String appSercret;

    @Autowired
    private LoanOrderMapper loanOrderMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private OrderMapper orderMapper;
    @Reference(version = "2.0.0")
    private TiedcardServiceInterface tiedcardServiceInterface;
    @Reference(version = "2.0.0")
    private IdentityServiceInterface identityServiceInterface;
    @Autowired
    private BizBankMapper bizBankMapper;




    /**
     * 修改订单状态
     * @return
     */
    public ResponseResult fangkuanpanduan(String state,String id) {
        Order order = orderMapper.selectorderid(id);

        Order ordera = new Order();
        ordera.setId(id);
        ordera.setState(state);

        orderMapper.updateByPrimaryKeySelective(ordera);
         if(!state.equals("0")){
             return new ResponseResult(StatusCode.ERROR,"失败");
        }
        rabbitTemplate.convertAndSend("order",order);
        return new ResponseResult(StatusCode.OK,"成功");
    }
    @Scheduled(cron = "0 0 1 * * ?")
    public void renzj(){
        List<LoanOrder> selectstarte = loanOrderMapper.selectstarte();
        for(LoanOrder a:selectstarte){
            String rescodee = a.getRescode();

            if(rescodee.equals("0")){
                String orderId = a.getOrderId();
                LoanOrder selectorderr = loanOrderMapper.selectorderr(orderId);
                String thirdtradeno = selectorderr.getThirdtradeno();

                String fangkuanrenzheng = this.fangkuanrenzheng(thirdtradeno);
                JSONObject jsonObject = JSON.parseObject(fangkuanrenzheng);
                JSONObject data = jsonObject.getJSONObject("data");
                String msgCode = (String) data.get("msgCode");
                if(!msgCode.equals("E200")){
                    selectorderr.setRescode("2");
                    loanOrderMapper.updateByPrimaryKeySelective(selectorderr);
                    Order selectorderid = orderMapper.selectByPrimaryKey(orderId);
                    selectorderid.setState("8");
                    orderMapper.updateByPrimaryKeySelective(selectorderid);

                }
                selectorderr.setRescode("1");
                loanOrderMapper.updateByPrimaryKeySelective(selectorderr);

            }
        }


    }






    /**
     * 放款认证接口
     * @param thirdTradeNo
     * @return
     */

    public String fangkuanrenzheng(String thirdTradeNo) {
        String url="http://10.1.74.19:8080/api/common/queryTradeResult";
        Map<String,String> map = new HashMap<>();
        map.put("merId",merId);
        map.put("thirdTradeNo",thirdTradeNo);
        String sign = MD5Sign.sign(map,appSercret);
        map.put("sign",sign);
        String str = HttpClientUtils.httpPost(url,map);
        return str;
    }
    /**
     * 调用银行放款
     * @param name
     * @param cardId
     * @param accNo
     * @param bankName
     * @param bankShortName
     * @param merTradeNo
     * @param amount
     * @return
     */



}
