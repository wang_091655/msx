package com.msx.trade.service.impl;

import com.msx.pojo.*;
import com.msx.trade.mapper.BillOrderMapper;
import com.msx.trade.mapper.OrderMapper;
import com.msx.trade.mapper.SmsMapper;
import com.msx.util.IdWorker;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class Zhuangdanserviceimpl {
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private BillOrderMapper billOrderMapper;
    @Autowired
    private SmsMapper smsMapper;
    @Autowired
    private OrderMapper orderMapper;

    @Scheduled(cron = "0 0 1 * * ?")
    public void sczd(){
        List<Order> orders = orderMapper.selectAll();
        for(Order order:orders){
            String state = order.getState();
            if(state.equals("4")){
                //生成账单数据
                BillOrder billOrder = new BillOrder();
                billOrder.setId(idWorker.nextId()+"");
                billOrder.setOrderId(order.getId());
                billOrder.setWhichIs(0);
                //计算每期还款金额
                //几期
                Integer period = order.getPeriod();
                String s = period.toString();
                //借款总金额
                BigDecimal receiveMoney = order.getReceiveMoney();
                //每期利率
                BigDecimal everyInterest = order.getEveryInterest();


                BigDecimal bigDecimal2 = null;
                bigDecimal2 = receiveMoney.multiply(everyInterest);

                BigDecimal bigDecimal4 = new BigDecimal(s);
                BigDecimal bigDecimal5 = null;
                bigDecimal5=receiveMoney.divide(bigDecimal4);
                BigDecimal bigDecimal6 = null;
                bigDecimal6=bigDecimal5.add(bigDecimal2);
                billOrder.setWhichMoney(bigDecimal6);
                billOrder.setCreateTime(new Date());
                //月数加1

                billOrder.setUserName(order.getUserName());
                billOrder.setUserMobile(order.getUserMobile());
                billOrder.setUserId(order.getUserId());
                billOrder.setState("0");
                billOrder.setIsDelete("0");

                for (int i=1;i<=period;i++){

                    billOrder.setId(idWorker.nextId()+"");
                    billOrder.setWhichIs(billOrder.getWhichIs()+1);
                    Date datea = DateUtils.addMonths(new Date(),i);
                    billOrder.setEndTime(datea);
                    billOrderMapper.insertSelective(billOrder);
                    billOrder.setCreateTime(datea);
                }
                this.smsadd(order);
                order.setState("5");
                orderMapper.updateByPrimaryKeySelective(order);
            }
        }






    }




    /**
     * 添加短信记录
     */

    public void smsadd(Order order){

        Sms sms = new Sms();
        sms.setContent("放款成功");
        sms.setMobile(order.getUserMobile());
        sms.setSendTime(new Date());
        sms.setSmsType("放款");
        sms.setUserId(order.getUserId());
        sms.setIsDelete("0");
        smsMapper.insert(sms);
    }
}
