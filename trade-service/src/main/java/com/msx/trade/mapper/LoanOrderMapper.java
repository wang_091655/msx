package com.msx.trade.mapper;

import com.msx.pojo.LoanOrder;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LoanOrderMapper extends IBaseMapper<LoanOrder> {
   @Select("SELECT * from tb_loan_order where user_id=#{userid}")
   LoanOrder selectuserid(Integer userid);
   @Select("SELECT * from tb_loan_order WHERE resCode='0'")
   public List<LoanOrder> selectstarte();
   @Select(" SELECT * from tb_loan_order WHERE order_id=#{orderId}")
   LoanOrder selectorderr(String orderId);
}