package com.msx.trade.mapper;

import com.msx.pojo.Identity;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;

public interface IdentityMapper extends IBaseMapper<Identity> {

}