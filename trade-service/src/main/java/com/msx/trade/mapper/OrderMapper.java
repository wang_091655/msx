package com.msx.trade.mapper;

import com.msx.pojo.Order;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface OrderMapper extends IBaseMapper<Order> {
    @Select("select * from tb_order where id = #{id}")
     Order selectorderid(String id);
    @Select("select * from tb_order where user_id = #{uid}")
    Order uidservice(Integer uid);

}