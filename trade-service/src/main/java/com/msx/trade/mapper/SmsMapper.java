package com.msx.trade.mapper;

import com.msx.pojo.Sms;
import com.msx.trade.base.IBaseMapper;

public interface SmsMapper extends IBaseMapper<Sms> {
}