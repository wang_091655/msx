package com.msx.trade.mapper;

import com.msx.pojo.BankShort;
import com.msx.pojo.BizBank;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Select;

public interface BizBankMapper extends IBaseMapper<BizBank> {
    @Select("SELECT * from biz_bank_info where id=#{bankid}")
    BizBank selectbankshort(Integer bankid);
}