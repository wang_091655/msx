package com.msx.trade.mapper;

import com.msx.pojo.BillOrder;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BillOrderMapper extends IBaseMapper<BillOrder> {
    public BillOrder selectQishu(@Param("bid") String bid);
    List<BillOrder> selectfindAll(@Param("id") Integer id);
}