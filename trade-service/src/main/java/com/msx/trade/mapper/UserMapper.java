package com.msx.trade.mapper;

import com.msx.pojo.User;
import com.msx.trade.base.IBaseMapper;

public interface UserMapper extends IBaseMapper<User> {
}