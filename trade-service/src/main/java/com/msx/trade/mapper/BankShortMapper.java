package com.msx.trade.mapper;

import com.msx.pojo.BankShort;
import com.msx.trade.base.IBaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface BankShortMapper extends IBaseMapper<BankShort> {

}