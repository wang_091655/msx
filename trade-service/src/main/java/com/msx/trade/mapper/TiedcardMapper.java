package com.msx.trade.mapper;

import com.msx.pojo.Identity;
import com.msx.pojo.Tiedcard;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;

public interface TiedcardMapper extends IBaseMapper<Tiedcard> {

}