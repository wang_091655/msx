package com.msx.trade.mapper;

import com.msx.pojo.RepaymentOrder;
import com.msx.trade.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RepaymentOrderMapper extends IBaseMapper<RepaymentOrder> {
    public RepaymentOrder selectRepay(@Param("userid") Integer userid);
    @Select("SELECT * from tb_repayment_order WHERE trade_state='0'")
    public RepaymentOrder selectorder(String orderId);
}