package com.msx.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "biz_bank_info")
public class BizBank implements Serializable {
    @Id
    private Long id;

    /**
     * 银行名称
     */
    @Column(name = "bank_name")
    private String bankName;

    /**
     * 银行简称
     */
    @Column(name = "bank_short_name")
    private String bankShortName;

    @Column(name = "bank_logo")
    private String bankLogo;

    /**
     * 是否删除(0删除，1未删除)
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "update_by")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取银行名称
     *
     * @return bank_name - 银行名称
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * 设置银行名称
     *
     * @param bankName 银行名称
     */
    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    /**
     * 获取银行简称
     *
     * @return bank_short_name - 银行简称
     */
    public String getBankShortName() {
        return bankShortName;
    }

    /**
     * 设置银行简称
     *
     * @param bankShortName 银行简称
     */
    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName == null ? null : bankShortName.trim();
    }

    /**
     * @return bank_logo
     */
    public String getBankLogo() {
        return bankLogo;
    }

    /**
     * @param bankLogo
     */
    public void setBankLogo(String bankLogo) {
        this.bankLogo = bankLogo == null ? null : bankLogo.trim();
    }

    /**
     * 获取是否删除(0删除，1未删除)
     *
     * @return is_delete - 是否删除(0删除，1未删除)
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除(0删除，1未删除)
     *
     * @param isDelete 是否删除(0删除，1未删除)
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}