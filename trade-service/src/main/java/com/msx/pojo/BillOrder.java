package com.msx.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_bill_order")
public class BillOrder implements Serializable {
    /**
     * ID 前缀HK_
     */
    @Id
    private String id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private String orderId;

    /**
     * 当前是第几期
     */
    @Column(name = "which_is")
    private Integer whichIs;

    /**
     * 当期应还金额
     */
    @Column(name = "which_money")
    private BigDecimal whichMoney;

    /**
     * 开始时间
     */

    @Column(name = "create_time")
    private Date createTime;

    /**
     * 还款时间
     */

    @Column(name = "end_time")
    private Date endTime;

    /**
     * 用户姓名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 用户手机号
     */
    @Column(name = "user_mobile")
    private String userMobile;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 账单状态 0:待还款1:已还款2:逾期3:未到期
     */
    private String state;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID 前缀HK_
     *
     * @return id - ID 前缀HK_
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID 前缀HK_
     *
     * @param id ID 前缀HK_
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 获取当前是第几期
     *
     * @return which_is - 当前是第几期
     */
    public Integer getWhichIs() {
        return whichIs;
    }

    /**
     * 设置当前是第几期
     *
     * @param whichIs 当前是第几期
     */
    public void setWhichIs(Integer whichIs) {
        this.whichIs = whichIs;
    }

    /**
     * 获取当期应还金额
     *
     * @return which_money - 当期应还金额
     */
    public BigDecimal getWhichMoney() {
        return whichMoney;
    }

    /**
     * 设置当期应还金额
     *
     * @param whichMoney 当期应还金额
     */
    public void setWhichMoney(BigDecimal whichMoney) {
        this.whichMoney = whichMoney;
    }

    /**
     * 获取开始时间
     *
     * @return create_time - 开始时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置开始时间
     *
     * @param createTime 开始时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取还款时间
     *
     * @return end_time - 还款时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置还款时间
     *
     * @param endTime 还款时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取用户姓名
     *
     * @return user_name - 用户姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户姓名
     *
     * @param userName 用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取用户手机号
     *
     * @return user_mobile - 用户手机号
     */
    public String getUserMobile() {
        return userMobile;
    }

    /**
     * 设置用户手机号
     *
     * @param userMobile 用户手机号
     */
    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile == null ? null : userMobile.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取账单状态 0:待还款1:已还款2:逾期3:未到期
     *
     * @return state - 账单状态 0:待还款1:已还款2:逾期3:未到期
     */
    public String getState() {
        return state;
    }

    /**
     * 设置账单状态 0:待还款1:已还款2:逾期3:未到期
     *
     * @param state 账单状态 0:待还款1:已还款2:逾期3:未到期
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}