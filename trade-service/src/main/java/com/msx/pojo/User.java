package com.msx.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "msx_user")
public class User implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String headportrait;

    /**
     * 注册时间
     */
    @Column(name = "createa_time")
    private Date createaTime;

    /**
     * 最后修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 身份认证 0:未认证 1:已认证
     */
    @Column(name = "user_authentication")
    private String userAuthentication;

    /**
     * 联系人认证 0:未认证 1:已认证
     */
    @Column(name = "linkman_authentication")
    private String linkmanAuthentication;

    /**
     * 绑卡认证 0:未认证 1:已认证
     */
    @Column(name = "card_authentication")
    private String cardAuthentication;

    /**
     * 通讯录认证 0:未认证 1:已认证
     */
    @Column(name = "addressbook_authentication")
    private String addressbookAuthentication;

    /**
     * 运营商认证 0:未认证 1:已认证
     */
    @Column(name = "operator_authentication")
    private String operatorAuthentication;

    /**
     * 淘宝认证 0:未认证 1:已认证
     */
    @Column(name = "taobao_authentication")
    private String taobaoAuthentication;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取姓名
     *
     * @return user_name - 姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置姓名
     *
     * @param userName 姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取昵称
     *
     * @return nickname - 昵称
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 设置昵称
     *
     * @param nickname 昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    /**
     * 获取头像
     *
     * @return headportrait - 头像
     */
    public String getHeadportrait() {
        return headportrait;
    }

    /**
     * 设置头像
     *
     * @param headportrait 头像
     */
    public void setHeadportrait(String headportrait) {
        this.headportrait = headportrait == null ? null : headportrait.trim();
    }

    /**
     * 获取注册时间
     *
     * @return createa_time - 注册时间
     */
    public Date getCreateaTime() {
        return createaTime;
    }

    /**
     * 设置注册时间
     *
     * @param createaTime 注册时间
     */
    public void setCreateaTime(Date createaTime) {
        this.createaTime = createaTime;
    }

    /**
     * 获取最后修改时间
     *
     * @return update_time - 最后修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置最后修改时间
     *
     * @param updateTime 最后修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取身份认证 0:未认证 1:已认证
     *
     * @return user_authentication - 身份认证 0:未认证 1:已认证
     */
    public String getUserAuthentication() {
        return userAuthentication;
    }

    /**
     * 设置身份认证 0:未认证 1:已认证
     *
     * @param userAuthentication 身份认证 0:未认证 1:已认证
     */
    public void setUserAuthentication(String userAuthentication) {
        this.userAuthentication = userAuthentication == null ? null : userAuthentication.trim();
    }

    /**
     * 获取联系人认证 0:未认证 1:已认证
     *
     * @return linkman_authentication - 联系人认证 0:未认证 1:已认证
     */
    public String getLinkmanAuthentication() {
        return linkmanAuthentication;
    }

    /**
     * 设置联系人认证 0:未认证 1:已认证
     *
     * @param linkmanAuthentication 联系人认证 0:未认证 1:已认证
     */
    public void setLinkmanAuthentication(String linkmanAuthentication) {
        this.linkmanAuthentication = linkmanAuthentication == null ? null : linkmanAuthentication.trim();
    }

    /**
     * 获取绑卡认证 0:未认证 1:已认证
     *
     * @return card_authentication - 绑卡认证 0:未认证 1:已认证
     */
    public String getCardAuthentication() {
        return cardAuthentication;
    }

    /**
     * 设置绑卡认证 0:未认证 1:已认证
     *
     * @param cardAuthentication 绑卡认证 0:未认证 1:已认证
     */
    public void setCardAuthentication(String cardAuthentication) {
        this.cardAuthentication = cardAuthentication == null ? null : cardAuthentication.trim();
    }

    /**
     * 获取通讯录认证 0:未认证 1:已认证
     *
     * @return addressbook_authentication - 通讯录认证 0:未认证 1:已认证
     */
    public String getAddressbookAuthentication() {
        return addressbookAuthentication;
    }

    /**
     * 设置通讯录认证 0:未认证 1:已认证
     *
     * @param addressbookAuthentication 通讯录认证 0:未认证 1:已认证
     */
    public void setAddressbookAuthentication(String addressbookAuthentication) {
        this.addressbookAuthentication = addressbookAuthentication == null ? null : addressbookAuthentication.trim();
    }

    /**
     * 获取运营商认证 0:未认证 1:已认证
     *
     * @return operator_authentication - 运营商认证 0:未认证 1:已认证
     */
    public String getOperatorAuthentication() {
        return operatorAuthentication;
    }

    /**
     * 设置运营商认证 0:未认证 1:已认证
     *
     * @param operatorAuthentication 运营商认证 0:未认证 1:已认证
     */
    public void setOperatorAuthentication(String operatorAuthentication) {
        this.operatorAuthentication = operatorAuthentication == null ? null : operatorAuthentication.trim();
    }

    /**
     * 获取淘宝认证 0:未认证 1:已认证
     *
     * @return taobao_authentication - 淘宝认证 0:未认证 1:已认证
     */
    public String getTaobaoAuthentication() {
        return taobaoAuthentication;
    }

    /**
     * 设置淘宝认证 0:未认证 1:已认证
     *
     * @param taobaoAuthentication 淘宝认证 0:未认证 1:已认证
     */
    public void setTaobaoAuthentication(String taobaoAuthentication) {
        this.taobaoAuthentication = taobaoAuthentication == null ? null : taobaoAuthentication.trim();
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}