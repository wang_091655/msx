package com.msx.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_repayment_order")
public class RepaymentOrder implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private String orderId;

    /**
     * 交易订单号
     */
    @Column(name = "out_trade_no")
    private String outTradeNo;

    /**
     * 交易状态 0:处理中1:交易成功2:交易失败
     */
    @Column(name = "trade_state")
    private String tradeState;

    /**
     * 交易号码(第三方返回的结果)
     */
    @Column(name = "transaction_id")
    private String transactionId;

    /**
     * 姓名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 银行简称
     */
    @Column(name = "bank_short")
    private String bankShort;

    /**
     * 银行卡号
     */
    @Column(name = "bank_code")
    private String bankCode;

    /**
     * 银行名称
     */
    @Column(name = "bank_name")
    private String bankName;

    /**
     * 身份证号
     */
    @Column(name = "identity_code")
    private String identityCode;

    /**
     * 还款时间
     */
    @Column(name = "loan_time")
    private Date loanTime;

    /**
     * 还款金额
     */
    @Column(name = "refund_money")
    private BigDecimal refundMoney;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 获取交易订单号
     *
     * @return out_trade_no - 交易订单号
     */
    public String getOutTradeNo() {
        return outTradeNo;
    }

    /**
     * 设置交易订单号
     *
     * @param outTradeNo 交易订单号
     */
    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo == null ? null : outTradeNo.trim();
    }

    /**
     * 获取交易状态 0:处理中1:交易成功2:交易失败
     *
     * @return trade_state - 交易状态 0:处理中1:交易成功2:交易失败
     */
    public String getTradeState() {
        return tradeState;
    }

    /**
     * 设置交易状态 0:处理中1:交易成功2:交易失败
     *
     * @param tradeState 交易状态 0:处理中1:交易成功2:交易失败
     */
    public void setTradeState(String tradeState) {
        this.tradeState = tradeState == null ? null : tradeState.trim();
    }

    /**
     * 获取交易号码(第三方返回的结果)
     *
     * @return transaction_id - 交易号码(第三方返回的结果)
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * 设置交易号码(第三方返回的结果)
     *
     * @param transactionId 交易号码(第三方返回的结果)
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId == null ? null : transactionId.trim();
    }

    /**
     * 获取姓名
     *
     * @return user_name - 姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置姓名
     *
     * @param userName 姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取银行简称
     *
     * @return bank_short - 银行简称
     */
    public String getBankShort() {
        return bankShort;
    }

    /**
     * 设置银行简称
     *
     * @param bankShort 银行简称
     */
    public void setBankShort(String bankShort) {
        this.bankShort = bankShort == null ? null : bankShort.trim();
    }

    /**
     * 获取银行卡号
     *
     * @return bank_code - 银行卡号
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * 设置银行卡号
     *
     * @param bankCode 银行卡号
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode == null ? null : bankCode.trim();
    }

    /**
     * 获取银行名称
     *
     * @return bank_name - 银行名称
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * 设置银行名称
     *
     * @param bankName 银行名称
     */
    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    /**
     * 获取身份证号
     *
     * @return identity_code - 身份证号
     */
    public String getIdentityCode() {
        return identityCode;
    }

    /**
     * 设置身份证号
     *
     * @param identityCode 身份证号
     */
    public void setIdentityCode(String identityCode) {
        this.identityCode = identityCode == null ? null : identityCode.trim();
    }

    /**
     * 获取还款时间
     *
     * @return loan_time - 还款时间
     */
    public Date getLoanTime() {
        return loanTime;
    }

    /**
     * 设置还款时间
     *
     * @param loanTime 还款时间
     */
    public void setLoanTime(Date loanTime) {
        this.loanTime = loanTime;
    }

    /**
     * 获取还款金额
     *
     * @return refund_money - 还款金额
     */
    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    /**
     * 设置还款金额
     *
     * @param refundMoney 还款金额
     */
    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}