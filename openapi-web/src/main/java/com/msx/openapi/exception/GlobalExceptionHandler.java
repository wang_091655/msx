package com.msx.openapi.exception;

import com.msx.exception.BizException;
import com.msx.util.ResponseResult;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 统一异常处理类
 * @author dutao
 * @create: 2020-06-16 09:40
 */
@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {

    /**
     * 返回的Map对象会被@ResponseBody注解转换为JSON数据返回
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object handleException(HttpServletRequest request, Exception e){
        System.out.println("###出现异常！");
        if(e instanceof BizException){
            BizException bizException=(BizException)e;
            log.info(bizException);
            return ResponseResult.error(bizException.getCode(),bizException.getMessage());
        }else{
            log.info(e);
            return ResponseResult.error(e.getMessage());
        }
    }
}
