package com.msx.openapi.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.constants.StatusCode;
import com.msx.pojo.User;
import com.msx.user.service.UserProducerInterface;
import com.msx.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/user")
@Api(tags = "秒速侠接口文档(money)")
public class UserController {

    @Reference(version = "2.0.0")
    private UserProducerInterface userProducerInterface;
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("UserList")
    @ApiOperation(value = "用户查询列表")
    public ResponseResult UserList(){
        List<User> uList = userProducerInterface.findUser();
        return ResponseResult.success(uList);
    }
    //手机号 +  密码
    @PostMapping(value = "/login")
    @ApiOperation(value = "手机密码登录")
    public ResponseResult login(@RequestBody User user, HttpServletRequest request){

        System.out.println(user.getMobile()+"--------------");
        System.out.println(user.getPassword()+"+++++++++++++++++");
        if (StringUtils.isBlank(user.getMobile()) && StringUtils.isBlank(user.getPassword())){

            return new ResponseResult(StatusCode.ERROR,"请输入手机号,密码");
        }
        if (StringUtils.isBlank(user.getMobile())){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号");
        }
        if (StringUtils.isBlank(user.getPassword())){
            return new ResponseResult(StatusCode.ERROR,"请输入密码");
        }
        return userProducerInterface.login(user);
    }
    //注册 验证码
    @PostMapping("/register")
    public ResponseResult register(@RequestBody User user, @RequestParam(value = "code") String code){
        ResponseResult register = userProducerInterface.register(user, code);
        return register;
    }
    //忘记密码
    @PostMapping("/forget")
    public ResponseResult forget(@RequestBody User user, @RequestParam(value = "code") String code){
        System.out.println(user.getMobile()+"...................");
        System.out.println(user.getPassword()+"---------------");
        System.out.println(code+"+++++++++++++++");
        return userProducerInterface.forget(user,code);
    }
    //手机 验证码 登录
    @PostMapping("/codeLogin")
    public ResponseResult codeLogin(@RequestBody User user, @RequestParam(value = "code") String code){
        System.out.println(user.getMobile()+".............");
        System.out.println(code+"-------------");
        if (StringUtils.isBlank(user.getMobile()) && StringUtils.isBlank(code)){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号，验证码");
        }
        if (StringUtils.isBlank(user.getMobile())){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号呀!!!!!!");
        }
        if (StringUtils.isBlank(code)){
            return new ResponseResult(StatusCode.ERROR,"请输入验证码呀!!!!!");
        }
        System.out.println("mobile:"+user.getMobile());
        System.out.println("code:"+code);
        return userProducerInterface.codeLogin(user,code);
    }

    //获取验证码
    @PostMapping("/codeSms")
    public ResponseResult codeSms(String mobile){
        return userProducerInterface.codeSms(mobile);
    }

    //测试请求头是否有UUid
    @RequestMapping("/findAll")
    public void findAll(HttpServletRequest request){
        String tou = request.getHeader("tou");
        System.out.println("请求头："+tou);
    }

}
