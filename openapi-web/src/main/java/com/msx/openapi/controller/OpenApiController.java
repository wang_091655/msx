package com.msx.openapi.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.msx.credit.service.CreditService;
import com.msx.exception.BizException;
import com.msx.pojo.Product;
import com.msx.risk.service.RiskService;


import com.msx.user.service.UserProducerInterface;
import com.msx.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author msx
 * @create: 2020-06-15 16:02
 */
@RestController
@RequestMapping("/user")
@Api(tags = "秒速侠接口文档")
public class OpenApiController {

    @Reference(version = "2.0.0")
    private UserProducerInterface userService;

    @Reference(version = "2.0.0")
    private RiskService riskService;
    @Reference(retries =0,version = "2.0.0")
    private CreditService creditService;


    @GetMapping("/findUserList")
    @ApiOperation(value = "获取用户列表")
    public ResponseResult findUserList(){
        return ResponseResult.success(userService.findAll());
    }
    @GetMapping("/findBlackList")
    @ApiOperation(value = "获取黑名单列表")
    public ResponseResult findBlackList(){
        return ResponseResult.success(riskService.findAll());
    }

    @GetMapping("/findOrderList")
    @ApiOperation(value = "获取order列表")
    public ResponseResult findOrderList(){

        return ResponseResult.success(creditService.findAll());
    }

    @GetMapping("/getCode")
    @ApiOperation(value = "获取注册验证码")
    @ApiImplicitParams({@ApiImplicitParam(name = "mobile",value = "手机号")})
    public ResponseResult getCode(String mobile){
        return ResponseResult.success("66666");
    }




    /**
     * 异常处理测试
     */
    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String test(String name) throws Exception{
        System.out.println("异常处理测试！");
        if(name==null) {
            throw new BizException(500,"字段太短");
        }
        return "index";
    }
}
