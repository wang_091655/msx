package com.msx.openapi.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.exception.BizException;
import com.msx.user.service.*;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping("/usershow")
@Api(tags = "秒速侠接口文档(显示用户信息)")
@Log4j2
public class VerifyUsershowController {
    //淘宝认证
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private IdentityTaoBaoServiceInterface identityTaoBaoServiceInterface;

    //联系人
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private LinkManServiceInterface linkManServiceInterface;

    //运营商认证
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private OperatorServiceInterface tOperatorServiceInterface;

    //身份认证
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private UserIdentityHomeInterface userIdentityHomeInterface;

    //绑卡验证
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private TiedcardServiceInterface tiedcardServiceInterface;

    //用户信息
    @Reference(timeout = 6000,retries=0,version = "2.0.0")
    private UserProducerInterface userService;

    //redis
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private HttpServletRequest request;

    @GetMapping("/findById")
    @ApiOperation(value = "获取用户信息",notes = "获取用户信息",produces = "application/json")
    public ResponseResult findbyid(@RequestHeader(value = "tou") String tou){
        if (StringUtils.isBlank(tou)){
            return new ResponseResult().error(400,"请重新登录!");
        }


        log.info("请求头："+tou);

        String falg =  redisTemplate.opsForValue().get(tou).toString();

        if (StringUtils.isBlank(falg)){
            return new ResponseResult().error(StatusCode.ERROR,"请从新登录");
        }
        log.info("用户信息："+falg);

        return new ResponseResult(StatusCode.OK,"获取成功",userService.findById(Integer.valueOf(falg)));
    }


    @GetMapping("/findLinkManById")
    @ApiOperation(value = "获取用户联系人信息",notes = "获取用户联系人信息",produces = "application/json")
    public ResponseResult findLinkManById(String userId){
        if (StringUtils.isBlank(userId)){
            return new ResponseResult().error(StatusCode.ERROR,"信息为空");
        }
        return linkManServiceInterface.findLinkManByUserId(Integer.valueOf(userId));
    }

    @GetMapping("/findOperatorByUserId")
    @ApiOperation(value = "获取用户运营商信息",notes = "获取用户运营商信息",produces = "application/json")
    public ResponseResult findOperatorByUserId(String userId){
        if (StringUtils.isBlank(userId)){
            return new ResponseResult().error(StatusCode.ERROR,"信息为空");
        }
        return tOperatorServiceInterface.findOperatorByUserId(Integer.valueOf(userId));
    }

    @GetMapping("/findTaobaoByUserId")
    @ApiOperation(value = "获取用户淘宝信息",notes = "获取用户淘宝信息",produces = "application/json")
    public ResponseResult findTaobaoByUserId(String userId){
        if (StringUtils.isBlank(userId)){
            return new ResponseResult().error(StatusCode.ERROR,"信息为空");
        }
        return identityTaoBaoServiceInterface.findTaobaoByUserId(Integer.valueOf(userId));
    }

    @GetMapping("/findidentityByUserId")
    @ApiOperation(value = "获取用户身份证信息",notes = "获取用户身份证信息",produces = "application/json")
    public ResponseResult findidentityByUserId(String userId){
        if (StringUtils.isBlank(userId)){
            return new ResponseResult().error(StatusCode.ERROR,"信息为空");
        }
        return userIdentityHomeInterface.identityEcho(userId);
    }


    //上传图片
    @PostMapping("/upload")
    @ApiOperation(value = "上传图片",notes = "上传图片",produces = "application/x-www-form-urlencoded")
    public ResponseResult upload(MultipartFile file){
        log.info("图片内容:"+file.getOriginalFilename());

        //非空验证
        if (StringUtils.isBlank(file.getOriginalFilename())){
            throw new BizException(StatusCode.ERROR,"图片为空");
        }

        String originalFilename = file.getOriginalFilename();//获取文件名

        if (!StringUtils.isBlank(originalFilename)){
            log.info("发送："+originalFilename);
            return new ResponseResult(StatusCode.OK,originalFilename);
        }

        return new ResponseResult().error(StatusCode.ERROR,"上传有误");
    }

    }
