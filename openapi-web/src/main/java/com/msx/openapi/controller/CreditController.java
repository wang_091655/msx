package com.msx.openapi.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.credit.service.CreditService;
import com.msx.pojo.Product;
import com.msx.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/credit")
@Api(tags = "秒速侠接口文档(money)")
public class CreditController {

    @Reference(version = "2.0.0",timeout = 6000,retries=0)
    private CreditService creditService;


    @GetMapping("/findProductAll")
    @ApiOperation(value = "动态查询(产品列表)")
    public ResponseResult findProductAll(){

        List<Product> productAll = creditService.findProductAll();
        return  ResponseResult.success(productAll);
    }

    @GetMapping("/findProductById")
    @ApiOperation(value = "获取产品单个对象")
    @ApiImplicitParams({@ApiImplicitParam(name = "productId",value = "产品ID")})
    public ResponseResult findProductById(Integer productId){

        return ResponseResult.success(creditService.findProductById(productId));
    }

    @RequestMapping("/createOrder")

    public ResponseResult createOrder(Integer productId,Integer userId){
        ResponseResult order = creditService.createOrder(productId, userId);

        return order;
    }
}
