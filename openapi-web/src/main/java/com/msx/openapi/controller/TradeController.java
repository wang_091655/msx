package com.msx.openapi.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.msx.credit.service.CreditService;
import com.msx.pojo.BillOrder;
import com.msx.trade.service.Huankuanservice;

import com.msx.trade.service.Tradeservice;
import com.msx.util.ResponseResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/trade")
@RestController
@CrossOrigin
public class TradeController {
    @Reference(version = "2.0.0")
    private Huankuanservice huankuanservice;
    @Reference(version = "2.0.0")
    private Tradeservice tradeservice;
    @RequestMapping("/aaa")
    @ApiOperation(value = "获取bill列表")
    public ResponseResult aaa(String state,String id){

        return ResponseResult.success(tradeservice.fangkuan(state,id));
    }


    @RequestMapping("/huankuan")
    public ResponseResult huankuan(String bid){


        return ResponseResult.success(huankuanservice.huankuan(bid));
    }
    @RequestMapping("/zhuangdan")
    public ResponseResult zhuangdan(){


    return ResponseResult.success(huankuanservice.zhuangdanlb());

    }
    @RequestMapping("/hkyzm")
    public ResponseResult hkyzm(HttpServletRequest request){
        huankuanservice.hkyzm(request);

        return ResponseResult.success("成功");
    }
    @RequestMapping("/hkjk")
    public ResponseResult hkjk(HttpServletRequest request,String yzm,String bid){
        huankuanservice.hkjk(request,yzm,bid);
        return ResponseResult.success("成功");
    }

}
