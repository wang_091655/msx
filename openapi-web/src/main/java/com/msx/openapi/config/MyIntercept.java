package com.msx.openapi.config;

import org.springframework.beans.factory.annotation.Autowired;
;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;
@CrossOrigin
@Component
public class MyIntercept implements HandlerInterceptor {

        @Autowired
        RedisTemplate redisTemplate;

        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

                String tou = request.getHeader("tou");
                System.out.println("请求头" + tou + "拦截器----------");
                if (tou != null) {
                        return true;
                } else {
                        //response.sendRedirect("http://localhost:9110/page/login.html");
                        response.sendRedirect(request.getContextPath() + "page/login.html");
                        return false;
                }
        }
}
