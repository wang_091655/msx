package com.msx.sms.consumer;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.msx.credit.service.Orderservice;
import com.msx.pojo.*;
import com.msx.trade.mapper.BankShortMapper;
import com.msx.trade.mapper.BizBankMapper;
import com.msx.trade.mapper.LoanOrderMapper;
import com.msx.trade.mapper.OrderMapper;
import com.msx.trade.service.Huankuanservice;
import com.msx.trade.service.impl.Zhuangdanserviceimpl;
import com.msx.trade.utils.HttpClientUtils;
import com.msx.trade.utils.MD5Sign;
import com.msx.user.service.IdentityServiceInterface;
import com.msx.user.service.TiedcardServiceInterface;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tensquare
 * @create: 2020-05-22 19:18
 */
@Component
public class SmsConsumer {
    @Reference(version = "2.0.0")
    private TiedcardServiceInterface tiedcardServiceInterface;
    @Reference(version = "2.0.0")
    private IdentityServiceInterface identityServiceInterface;
    @Autowired
    private LoanOrderMapper loanOrderMapper;
    @Autowired
    private OrderMapper orderMapper;
   @Autowired
   private BizBankMapper bizBankMapper;
    private String merId;


    private String appSercret;
    @Autowired
    private Zhuangdanserviceimpl zhuangdanserviceimpl;


    @RabbitListener(queues = "order")
    public void loanOrderadd(Order order){

        LoanOrder loanOrder = new LoanOrder();
        loanOrder.setOrderId(order.getId());
        //获取第三方交易订单


        loanOrder.setRescode("0");
        loanOrder.setUserName(order.getUserName());
        Integer userId = order.getUserId();
        //到时候改 调接口
        Tiedcard selecttidecard = tiedcardServiceInterface.findByIduserid(userId);
        System.out.println(selecttidecard.toString()+"111111111111111111");
        Identity selectidentity = identityServiceInterface.selectidentity(userId);
        Integer bankid = selecttidecard.getBankId();
        System.out.println(bankid);
        BizBank selectbankshort = bizBankMapper.selectByPrimaryKey(bankid);
        loanOrder.setIdentityCode(selectidentity.getIdentityCode());
        loanOrder.setBankCode(selecttidecard.getBankCode());
        System.out.println(selectbankshort.getBankName()+selectbankshort.getBankShortName());
        loanOrder.setBankName(selectbankshort.getBankName());
        loanOrder.setBankShort(selectbankshort.getBankShortName());
        loanOrder.setLoanTime(new Date());
        loanOrder.setLoanMoney(order.getBorrowMoney());
        loanOrder.setUserId(userId);
        loanOrder.setIsDelete("0");
        loanOrderMapper.insertSelective(loanOrder);

        //修改订单状态为贷放款
        order.setState("3");
        orderMapper.updateByPrimaryKeySelective(order);
        //调用第三方 放款接口
        String state = order.getState();

        if(!state.equals("1")&& state.equals("3")){
            LoanOrder selectuserid = loanOrderMapper.selectuserid(userId);
            selectuserid.setRescode("2");
            loanOrderMapper.updateByPrimaryKeySelective(selectuserid);

        }
        String s = this.identityAuth(order.getUserName(), selectidentity.getIdentityCode(), selecttidecard.getBankCode(), selectbankshort.getBankName(), selectbankshort.getBankShortName(), order.getId(), order.getBorrowMoney());
        //获取银行放款第三方密码代码
        JSONObject jsonObject = JSON.parseObject(s);
        JSONObject data = jsonObject.getJSONObject("data");
        String thirdTradeNo = (String) data.get("thirdTradeNo");
        String resCode = (String) data.get("resCode");
        LoanOrder selectuserid = loanOrderMapper.selectuserid(userId);
        selectuserid.setThirdtradeno(thirdTradeNo);
        selectuserid.setRescode("0");
        loanOrderMapper.updateByPrimaryKeySelective(selectuserid);

        order.setState("4");
        orderMapper.updateByPrimaryKeySelective(order);


    }
    /**
     * 调用银行放款
     * @param name
     * @param cardId
     * @param accNo
     * @param bankName
     * @param bankShortName
     * @param merTradeNo
     * @param amount
     * @return
     */

    public String identityAuth(String name, String cardId, String accNo, String bankName, String bankShortName, String merTradeNo, BigDecimal amount) {
        String url="http://10.1.74.19:8080/api/common/substitute";
        Map<String,String> map = new HashMap<>();
        map.put("merId",merId);
        map.put("name",name);
        map.put("cardId",cardId);
        map.put("accNo",accNo);
        map.put("bankName",bankName);
        map.put("bankShortName",bankShortName);
        map.put("merTradeNo",merTradeNo);
        map.put("amount",amount+"");
        String sign = MD5Sign.sign(map,appSercret);
        map.put("sign",sign);
        String str = HttpClientUtils.httpPost(url,map);
        return str;
    }




}
