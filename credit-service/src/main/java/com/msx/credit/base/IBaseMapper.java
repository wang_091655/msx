package com.msx.credit.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.io.Serializable;

/**
 * 继承自己的MyMapper
 *
 * @author liuzh
 * @since 2015-09-06 21:53
 */
public interface IBaseMapper<T extends Serializable> extends Mapper<T>, MySqlMapper<T> {

}