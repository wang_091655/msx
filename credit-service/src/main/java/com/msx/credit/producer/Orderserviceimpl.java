package com.msx.credit.producer;

import com.msx.credit.service.Orderservice;
import com.msx.credit.service.impl.CreditServiceImpl;
import com.msx.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class Orderserviceimpl implements Orderservice {
    @Autowired
    private CreditServiceImpl creditService;
    @Override
    public Order uidorder(Integer uid) {

        return creditService.uidorder(uid);
    }

    @Override
    public void update(Order order) {
        creditService.updateorder(order);
    }

    @Override
    public Order selectorderid(String orderId) {
        return creditService.selectorderid(orderId);
    }
}
