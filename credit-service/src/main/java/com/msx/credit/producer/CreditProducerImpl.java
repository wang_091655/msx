package com.msx.credit.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.credit.service.CreditService;
import com.msx.credit.service.impl.CreditServiceImpl;
import com.msx.pojo.Order;
import com.msx.pojo.Product;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-16 11:12
 */
@Service(timeout = 6000,retries=0,version = "2.0.0")
public class CreditProducerImpl implements CreditService {

    @Autowired
    private CreditServiceImpl creditService;


    @Override
    public List<Order> findAll() {

        return creditService.findAll();
    }

    @Override
    public List<Product> findProductAll() {
        return creditService.findProductAll();
    }

    @Override
    public Product findProductById(Integer productId) {


        return creditService.findProductById(productId);
    }

    @Override
    public ResponseResult createOrder(Integer productId,Integer userId) {
        return creditService.createOrder(productId,userId);
    }

    @Override
    public List<Order> SelectByUserid2(Integer userId) {
        return null;
    }


}
