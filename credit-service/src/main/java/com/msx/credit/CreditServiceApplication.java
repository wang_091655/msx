package com.msx.credit;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.msx.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

/**0
 * @author msx
 * @create: 2020-06-13 16:45
 */
@SpringBootApplication
@EnableDubbo
@MapperScan("com.msx.credit.mapper")
public class CreditServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(CreditServiceApplication.class,args);
    }

    @Bean
    public IdWorker idWorker(){
        return new IdWorker();
    }
}
