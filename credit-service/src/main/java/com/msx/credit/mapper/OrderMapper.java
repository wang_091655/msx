package com.msx.credit.mapper;

import com.msx.credit.base.IBaseMapper;
import com.msx.pojo.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderMapper extends IBaseMapper<Order> {


    @Select("select * from tb_order where user_id = #{userId} and state in('0','1','2','3','4','5','6','8')")
    List<Order> selectByUserId(Integer userId);
    Order selectuid(@Param("uid")Integer uid);
    public Order selectorderid(@Param("orderId") String orderId);
}