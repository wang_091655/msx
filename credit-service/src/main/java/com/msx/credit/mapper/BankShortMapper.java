package com.msx.credit.mapper;

import com.msx.credit.base.IBaseMapper;
import com.msx.pojo.BankShort;

public interface BankShortMapper extends IBaseMapper<BankShort> {
}