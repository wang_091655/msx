package com.msx.credit.mapper;

import com.msx.credit.base.IBaseMapper;
import com.msx.pojo.Order;
import com.msx.pojo.Product;
import org.apache.ibatis.annotations.Select;

public interface ProductMapper extends IBaseMapper<Product> {

    @Select("select * from tb_product where id = #{productId}")
    Product findProductById(Integer productId);


}