package com.msx.credit.service.impl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.credit.mapper.OrderMapper;
import com.msx.credit.mapper.ProductMapper;
import com.msx.credit.service.CreditService;
import com.msx.pojo.Order;
import com.msx.pojo.Product;
import com.msx.pojo.User;
import com.msx.user.service.UserProducerInterface;
import com.msx.user.service.UserService;


import com.msx.util.IdWorker;
import com.msx.util.ResponseResult;
import org.springframework.amqp.rabbit.core.RabbitManagementTemplate;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.bind.PrintConversionEvent;
import java.util.*;

/**
 * @author dutao
 * @create: 2020-06-15 20:21
 */
@Service
public class CreditServiceImpl{

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ProductMapper productMapper;

   @Reference(version = "2.0.0")
    private UserProducerInterface userProducerInterface;

   @Autowired
   private RabbitMessagingTemplate rabbitMessagingTemplate;


    @Autowired
    private IdWorker idWorker;

    @Autowired
    private RedisTemplate redisTemplate;

    public List<Order> findAll() {
        return orderMapper.select(null);
    }

    /**
     * @author wujiangtao
     * @create: 2020-06-16 20:21
     * 产品列表
     */
    public List<Product> findProductAll() {
        return productMapper.selectAll();
    }


    /**
     * @author wujiangtao
     * @param productId
     * 产品对象
     */
    public Product findProductById(Integer productId){

        Product product = productMapper.findProductById(productId);
        System.out.println("啊啊啊啊"+product);
        return product;
    }

    /*
    *   @author 滕飞
    *   @create:2020-06-18 10:48
    *   创建订单
    *     * */

    public ResponseResult createOrder(Integer productId, Integer userId){
        System.out.println("家里了额");

        List<Order>  list2 = orderMapper.selectByUserId(userId);

        if(CollectionUtils.isNotEmpty(list2)){
            return ResponseResult.error("订单提交失败,您有一笔未完成的订单");
        }


            //查找商品
            Product product = productMapper.findProductById(productId);
            //创建订单对象
            Order order = new Order();
            //雪花算法赋值id
            order.setId(idWorker.nextId()+"");
            //产品名称
            order.setProductName(product.getProductName());
            //产品描述
            order.setProductDesc(product.getProductDesc());
            //周期
            order.setPeriod(product.getPeriod());
            //每期利率
            order.setEveryInterest(product.getEveryInterest());
            //罚息费率
            order.setFaCharge(product.getFaCharge1());
            //借款金额
            order.setBorrowMoney(product.getMoney());
            //到账金额
            order.setReceiveMoney(product.getMoney());
            //获取当前时间
            Date date = new Date();
            //创建时间
            order.setCreateTime(date);
            //状态
            order.setState("0");

            User user = userProducerInterface.findById(userId);
            //用户名
            order.setUserName(user.getUserName());
            //用户手机号
            order.setUserMobile(user.getMobile());
            //用户id
            order.setUserId(user.getId());
            //逻辑删除
            order.setIsDelete("0");

            //添加到订单表
             orderMapper.insert(order);

            // 存入MQ
        /*try {
            Map<String, String> map = new HashMap<>();
            map.put("ordernumber",order.getId());//订单号
            map.put("userId",order.getUserId()+"");//用户id
            map.put("userName",order.getUserName());//用户名
            rabbitMessagingTemplate.convertAndSend("order",map);
        }catch (Exception ex){
            ex.printStackTrace();
        }*/



        return ResponseResult.success("订单提交成功");

    }

    public Order uidorder(Integer uid){
        return orderMapper.selectuid(uid);

    }
    public void updateorder(Order order){
        orderMapper.updateByPrimaryKeySelective(order);
    }
    public Order selectorderid(String orderId){
        return orderMapper.selectorderid(orderId);
    }







}
