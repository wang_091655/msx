package com.msx.credit.service;

import com.msx.pojo.Order;
import com.msx.pojo.Product;
import com.msx.util.ResponseResult;

import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-15 18:41
 */
public interface CreditService {
    List<Order> findAll();

    List<Product> findProductAll();

    Product findProductById(Integer productId);

    ResponseResult createOrder(Integer productId, Integer userId);

    List<Order> SelectByUserid2(Integer userId);
}
