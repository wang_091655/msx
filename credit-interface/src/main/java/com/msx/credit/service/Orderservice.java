package com.msx.credit.service;

import com.msx.pojo.Order;

import java.util.List;

public interface Orderservice {
    public Order uidorder(Integer uid);
    public void update(Order order);
    public Order selectorderid(String orderId);
}
