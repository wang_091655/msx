package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "tb_order")
public class Order implements Serializable {
    /**
     * ID 前缀O_
     */
    private String id;

    /**
     * 产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 产品描述
     */
    @Column(name = "product_desc")
    private String productDesc;

    /**
     * 周期
     */
    private Integer period;

    /**
     * 每期利率
     */
    @Column(name = "every_interest")
    private BigDecimal everyInterest;

    /**
     * 罚息费率
     */
    @Column(name = "fa_charge")
    private BigDecimal faCharge;

    /**
     * 借款金额
     */
    @Column(name = "borrow_money")
    private BigDecimal borrowMoney;

    /**
     * 到账金额
     */
    @Column(name = "receive_money")
    private BigDecimal receiveMoney;

    /**
     * 逾期天数
     */
    private Integer overdue;

    /**
     * 罚息总金额
     */
    private BigDecimal fine;

    /**
     * 应还金额
     */
    @Column(name = "should_money")
    private BigDecimal shouldMoney;

    /**
     * 申请时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 订单状态 0:待审核1:审核通过2:审核未通过3:待放款4:放款中5:待还款6:逾期7:已还款8:放款失败
     */
    private String state;

    /**
     * 用户姓名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 用户手机号
     */
    @Column(name = "user_mobile")
    private String userMobile;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID 前缀O_
     *
     * @return id - ID 前缀O_
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID 前缀O_
     *
     * @param id ID 前缀O_
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取产品名称
     *
     * @return product_name - 产品名称
     */
    public String getProductName() {
        return productName;
    }

    /**
     * 设置产品名称
     *
     * @param productName 产品名称
     */
    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    /**
     * 获取产品描述
     *
     * @return product_desc - 产品描述
     */
    public String getProductDesc() {
        return productDesc;
    }

    /**
     * 设置产品描述
     *
     * @param productDesc 产品描述
     */
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc == null ? null : productDesc.trim();
    }

    /**
     * 获取周期
     *
     * @return period - 周期
     */
    public Integer getPeriod() {
        return period;
    }

    /**
     * 设置周期
     *
     * @param period 周期
     */
    public void setPeriod(Integer period) {
        this.period = period;
    }

    /**
     * 获取每期利率
     *
     * @return every_interest - 每期利率
     */
    public BigDecimal getEveryInterest() {
        return everyInterest;
    }

    /**
     * 设置每期利率
     *
     * @param everyInterest 每期利率
     */
    public void setEveryInterest(BigDecimal everyInterest) {
        this.everyInterest = everyInterest;
    }

    /**
     * 获取罚息费率
     *
     * @return fa_charge - 罚息费率
     */
    public BigDecimal getFaCharge() {
        return faCharge;
    }

    /**
     * 设置罚息费率
     *
     * @param faCharge 罚息费率
     */
    public void setFaCharge(BigDecimal faCharge) {
        this.faCharge = faCharge;
    }

    /**
     * 获取借款金额
     *
     * @return borrow_money - 借款金额
     */
    public BigDecimal getBorrowMoney() {
        return borrowMoney;
    }

    /**
     * 设置借款金额
     *
     * @param borrowMoney 借款金额
     */
    public void setBorrowMoney(BigDecimal borrowMoney) {
        this.borrowMoney = borrowMoney;
    }

    /**
     * 获取到账金额
     *
     * @return receive_money - 到账金额
     */
    public BigDecimal getReceiveMoney() {
        return receiveMoney;
    }

    /**
     * 设置到账金额
     *
     * @param receiveMoney 到账金额
     */
    public void setReceiveMoney(BigDecimal receiveMoney) {
        this.receiveMoney = receiveMoney;
    }

    /**
     * 获取逾期天数
     *
     * @return overdue - 逾期天数
     */
    public Integer getOverdue() {
        return overdue;
    }

    /**
     * 设置逾期天数
     *
     * @param overdue 逾期天数
     */
    public void setOverdue(Integer overdue) {
        this.overdue = overdue;
    }

    /**
     * 获取罚息总金额
     *
     * @return fine - 罚息总金额
     */
    public BigDecimal getFine() {
        return fine;
    }

    /**
     * 设置罚息总金额
     *
     * @param fine 罚息总金额
     */
    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    /**
     * 获取应还金额
     *
     * @return should_money - 应还金额
     */
    public BigDecimal getShouldMoney() {
        return shouldMoney;
    }

    /**
     * 设置应还金额
     *
     * @param shouldMoney 应还金额
     */
    public void setShouldMoney(BigDecimal shouldMoney) {
        this.shouldMoney = shouldMoney;
    }

    /**
     * 获取申请时间
     *
     * @return create_time - 申请时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置申请时间
     *
     * @param createTime 申请时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取订单状态 0:待审核1:审核通过2:审核未通过3:待放款4:放款中5:待还款6:逾期7:已还款8:放款失败
     *
     * @return state - 订单状态 0:待审核1:审核通过2:审核未通过3:待放款4:放款中5:待还款6:逾期7:已还款8:放款失败
     */
    public String getState() {
        return state;
    }

    /**
     * 设置订单状态 0:待审核1:审核通过2:审核未通过3:待放款4:放款中5:待还款6:逾期7:已还款8:放款失败
     *
     * @param state 订单状态 0:待审核1:审核通过2:审核未通过3:待放款4:放款中5:待还款6:逾期7:已还款8:放款失败
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取用户姓名
     *
     * @return user_name - 用户姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户姓名
     *
     * @param userName 用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取用户手机号
     *
     * @return user_mobile - 用户手机号
     */
    public String getUserMobile() {
        return userMobile;
    }

    /**
     * 设置用户手机号
     *
     * @param userMobile 用户手机号
     */
    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile == null ? null : userMobile.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}