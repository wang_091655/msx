package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "tb_product")
public class Product implements Serializable {
    /**
     * ID
     */
    private Integer id;

    /**
     * 产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 产品描述
     */
    @Column(name = "product_desc")
    private String productDesc;

    /**
     * 周期
     */
    private Integer period;

    /**
     * 每期利率
     */
    @Column(name = "every_interest")
    private BigDecimal everyInterest;

    /**
     * 罚息费率
     */
    @Column(name = "fa_charge1")
    private BigDecimal faCharge1;

    /**
     * 借款金额
     */
    private BigDecimal money;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取产品名称
     *
     * @return product_name - 产品名称
     */
    public String getProductName() {
        return productName;
    }

    /**
     * 设置产品名称
     *
     * @param productName 产品名称
     */
    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    /**
     * 获取产品描述
     *
     * @return product_desc - 产品描述
     */
    public String getProductDesc() {
        return productDesc;
    }

    /**
     * 设置产品描述
     *
     * @param productDesc 产品描述
     */
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc == null ? null : productDesc.trim();
    }

    /**
     * 获取周期
     *
     * @return period - 周期
     */
    public Integer getPeriod() {
        return period;
    }

    /**
     * 设置周期
     *
     * @param period 周期
     */
    public void setPeriod(Integer period) {
        this.period = period;
    }

    /**
     * 获取每期利率
     *
     * @return every_interest - 每期利率
     */
    public BigDecimal getEveryInterest() {
        return everyInterest;
    }

    /**
     * 设置每期利率
     *
     * @param everyInterest 每期利率
     */
    public void setEveryInterest(BigDecimal everyInterest) {
        this.everyInterest = everyInterest;
    }

    /**
     * 获取罚息费率
     *
     * @return fa_charge1 - 罚息费率
     */
    public BigDecimal getFaCharge1() {
        return faCharge1;
    }

    /**
     * 设置罚息费率
     *
     * @param faCharge1 罚息费率
     */
    public void setFaCharge1(BigDecimal faCharge1) {
        this.faCharge1 = faCharge1;
    }

    /**
     * 获取借款金额
     *
     * @return money - 借款金额
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * 设置借款金额
     *
     * @param money 借款金额
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}