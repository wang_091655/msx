package com.msx.user.service;

import com.msx.pojo.Tiedcard;
import com.msx.util.ResponseResult;

/**
 * 绑卡认证
 * 1.发送信息
 * 2.确认
 */
public interface TiedcardServiceInterface {
    //填写信息获取验证码
    public ResponseResult TiedcardOut(Tiedcard tiedcard);
    //绑定信息 确认绑定
    public ResponseResult indeupdate(Tiedcard tiedcard);
    //根据id获取信息
    public ResponseResult findById(String id);

    public Tiedcard findByIduserid(Integer userid);

}
