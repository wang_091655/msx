package com.msx.user.service;

import com.msx.pojo.Operator;
import com.msx.pojo.Sms;
import com.msx.util.ResponseResult;

/**
 * 运营商认证口
 */
public interface OperatorServiceInterface {
    //发送信息
    public ResponseResult OutSms(Sms sms);
    //认证
    public ResponseResult IdentityOperator(Operator operator);
    //根据用户id查询运营商表
    public ResponseResult findOperatorByUserId(Integer userId);
}
