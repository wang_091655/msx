package com.msx.user.service;

import com.msx.pojo.Linkman;
import com.msx.util.ResponseResult;

/**
 * 联系人认证接口
 */
public interface LinkManServiceInterface {
    public ResponseResult identificationTbLinkMan(Linkman tbLinkman);

    //根据用户id查询联系人表
    ResponseResult findLinkManByUserId(Integer userId);
}
