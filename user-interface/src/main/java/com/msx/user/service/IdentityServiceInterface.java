package com.msx.user.service;

import com.msx.pojo.Identity;
import com.msx.pojo.User;

/**
 * 认证身份证
 * 请使用UserIdentityHomeInterface
 */
public interface IdentityServiceInterface {
     //提交认证
     public String UserUpdate(User user);
     //正面 实名认证接口
     public String identityAuth(String name, String cardId, String base64FrontPic);
     //反面
     public String identitybank(String name, String cardId, String base64FrontPic);
     //orc
     public String identityOrc(String name, String cardId, String base64FrontPic);
     //活体
     public String liveAuth(String name, String cardId, String base64FrontPic);


     public Identity selectidentity(Integer userid);


}
