package com.msx.user.service;

import com.msx.pojo.Admin;
import com.msx.pojo.User;

import java.util.List;

/**
 * @author msx
 * @create: 2020-06-13 16:46
 */
public interface UserService {
    List<Admin> findAll();

    User findById(Integer id);
}
