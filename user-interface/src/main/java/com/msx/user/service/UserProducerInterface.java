package com.msx.user.service;

import com.msx.pojo.Admin;
import com.msx.pojo.User;
import com.msx.util.ResponseResult;

import java.util.List;

/**
 * @author msx
 * @create: 2020-06-13 16:46
 */
public interface UserProducerInterface {
    List<Admin> findAll();

    List<User> findUser();

    //手机号 密码登录
    ResponseResult login(User user);

    //注册
    ResponseResult register(User user, String code);

    User findById(Integer id);

    //手机 验证码 登录
    ResponseResult codeLogin(User user, String code);

    //忘记密码
    ResponseResult forget(User user, String code);

    //获取验证码
    ResponseResult codeSms(String mobile);
}
