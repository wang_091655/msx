package com.msx.user.service;

import com.msx.util.ResponseResult;

/**
 * 身份总认证
 */
public interface UserIdentityHomeInterface {
    public ResponseResult UserIdentityHome(
            String id,
            String name,
            String identitycode,
            String frontImage,
            String backImage,
            String liveImage
    );

    //orc
    public ResponseResult identityOrc(String base64FrontPic);

    //回显绑定银行卡信息
    public ResponseResult identityEcho(String useid);
}
