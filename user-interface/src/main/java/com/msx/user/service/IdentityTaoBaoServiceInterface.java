package com.msx.user.service;


import com.msx.pojo.Taobao;
import com.msx.util.ResponseResult;

/**
 * 淘宝认证接口
 */
public interface IdentityTaoBaoServiceInterface {
    public ResponseResult IdentityTaoBaoService(Taobao taobao);
    //根据用户id查询淘宝表;
    public ResponseResult findTaobaoByUserId(Integer userId);
}
