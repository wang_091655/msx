package com.msx.user.service;

import com.msx.util.ResponseResult;

/**
 * 通讯录
 */
public interface AddressbookServiceInterface {
    public ResponseResult update(String id);
}
