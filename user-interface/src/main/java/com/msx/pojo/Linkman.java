package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "tb_linkman")
public class Linkman implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 紧急联系人
     */
    @Column(name = "urgency_name")
    private String urgencyName;

    /**
     * 紧急联系人手机号
     */
    @Column(name = "urgency_mobile")
    private String urgencyMobile;

    /**
     * 紧急联系人关系 1:父亲2:母亲3:配偶
     */
    @Column(name = "urgency_relation")
    private String urgencyRelation;

    /**
     * 第一联系人姓名
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * 第一联系人手机号
     */
    @Column(name = "first_mobile")
    private String firstMobile;

    /**
     * 第一联系人关系 4:兄弟5:姐妹6:朋友
     */
    @Column(name = "first_relation")
    private String firstRelation;

    /**
     * 第二联系人姓名
     */
    @Column(name = "second_name")
    private String secondName;

    /**
     * 第二联系人手机号
     */
    @Column(name = "second_mobile")
    private String secondMobile;

    /**
     * 第二联系人关系 7:朋友8:同学9:同事
     */
    @Column(name = "second_relation")
    private String secondRelation;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 认证时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取紧急联系人
     *
     * @return urgency_name - 紧急联系人
     */
    public String getUrgencyName() {
        return urgencyName;
    }

    /**
     * 设置紧急联系人
     *
     * @param urgencyName 紧急联系人
     */
    public void setUrgencyName(String urgencyName) {
        this.urgencyName = urgencyName == null ? null : urgencyName.trim();
    }

    /**
     * 获取紧急联系人手机号
     *
     * @return urgency_mobile - 紧急联系人手机号
     */
    public String getUrgencyMobile() {
        return urgencyMobile;
    }

    /**
     * 设置紧急联系人手机号
     *
     * @param urgencyMobile 紧急联系人手机号
     */
    public void setUrgencyMobile(String urgencyMobile) {
        this.urgencyMobile = urgencyMobile == null ? null : urgencyMobile.trim();
    }

    /**
     * 获取紧急联系人关系 1:父亲2:母亲3:配偶
     *
     * @return urgency_relation - 紧急联系人关系 1:父亲2:母亲3:配偶
     */
    public String getUrgencyRelation() {
        return urgencyRelation;
    }

    /**
     * 设置紧急联系人关系 1:父亲2:母亲3:配偶
     *
     * @param urgencyRelation 紧急联系人关系 1:父亲2:母亲3:配偶
     */
    public void setUrgencyRelation(String urgencyRelation) {
        this.urgencyRelation = urgencyRelation == null ? null : urgencyRelation.trim();
    }

    /**
     * 获取第一联系人姓名
     *
     * @return first_name - 第一联系人姓名
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 设置第一联系人姓名
     *
     * @param firstName 第一联系人姓名
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName == null ? null : firstName.trim();
    }

    /**
     * 获取第一联系人手机号
     *
     * @return first_mobile - 第一联系人手机号
     */
    public String getFirstMobile() {
        return firstMobile;
    }

    /**
     * 设置第一联系人手机号
     *
     * @param firstMobile 第一联系人手机号
     */
    public void setFirstMobile(String firstMobile) {
        this.firstMobile = firstMobile == null ? null : firstMobile.trim();
    }

    /**
     * 获取第一联系人关系 4:兄弟5:姐妹6:朋友
     *
     * @return first_relation - 第一联系人关系 4:兄弟5:姐妹6:朋友
     */
    public String getFirstRelation() {
        return firstRelation;
    }

    /**
     * 设置第一联系人关系 4:兄弟5:姐妹6:朋友
     *
     * @param firstRelation 第一联系人关系 4:兄弟5:姐妹6:朋友
     */
    public void setFirstRelation(String firstRelation) {
        this.firstRelation = firstRelation == null ? null : firstRelation.trim();
    }

    /**
     * 获取第二联系人姓名
     *
     * @return second_name - 第二联系人姓名
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * 设置第二联系人姓名
     *
     * @param secondName 第二联系人姓名
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName == null ? null : secondName.trim();
    }

    /**
     * 获取第二联系人手机号
     *
     * @return second_mobile - 第二联系人手机号
     */
    public String getSecondMobile() {
        return secondMobile;
    }

    /**
     * 设置第二联系人手机号
     *
     * @param secondMobile 第二联系人手机号
     */
    public void setSecondMobile(String secondMobile) {
        this.secondMobile = secondMobile == null ? null : secondMobile.trim();
    }

    /**
     * 获取第二联系人关系 7:朋友8:同学9:同事
     *
     * @return second_relation - 第二联系人关系 7:朋友8:同学9:同事
     */
    public String getSecondRelation() {
        return secondRelation;
    }

    /**
     * 设置第二联系人关系 7:朋友8:同学9:同事
     *
     * @param secondRelation 第二联系人关系 7:朋友8:同学9:同事
     */
    public void setSecondRelation(String secondRelation) {
        this.secondRelation = secondRelation == null ? null : secondRelation.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取认证时间
     *
     * @return create_time - 认证时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置认证时间
     *
     * @param createTime 认证时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}