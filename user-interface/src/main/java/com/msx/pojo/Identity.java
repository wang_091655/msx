package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "tb_identity")
public class Identity implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 身份证正面
     */
    @Column(name = "identity_front")
    private String identityFront;

    /**
     * 身份证反面
     */
    @Column(name = "identity_back")
    private String identityBack;

    /**
     * 活体认证照片
     */
    @Column(name = "living_certification")
    private String livingCertification;

    /**
     * 姓名
     */
    private String name;

    /**
     * 身份证号
     */
    @Column(name = "identity_code")
    private String identityCode;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 认证时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取身份证正面
     *
     * @return identity_front - 身份证正面
     */
    public String getIdentityFront() {
        return identityFront;
    }

    /**
     * 设置身份证正面
     *
     * @param identityFront 身份证正面
     */
    public void setIdentityFront(String identityFront) {
        this.identityFront = identityFront == null ? null : identityFront.trim();
    }

    /**
     * 获取身份证反面
     *
     * @return identity_back - 身份证反面
     */
    public String getIdentityBack() {
        return identityBack;
    }

    /**
     * 设置身份证反面
     *
     * @param identityBack 身份证反面
     */
    public void setIdentityBack(String identityBack) {
        this.identityBack = identityBack == null ? null : identityBack.trim();
    }

    /**
     * 获取活体认证照片
     *
     * @return living_certification - 活体认证照片
     */
    public String getLivingCertification() {
        return livingCertification;
    }

    /**
     * 设置活体认证照片
     *
     * @param livingCertification 活体认证照片
     */
    public void setLivingCertification(String livingCertification) {
        this.livingCertification = livingCertification == null ? null : livingCertification.trim();
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取身份证号
     *
     * @return identity_code - 身份证号
     */
    public String getIdentityCode() {
        return identityCode;
    }

    /**
     * 设置身份证号
     *
     * @param identityCode 身份证号
     */
    public void setIdentityCode(String identityCode) {
        this.identityCode = identityCode == null ? null : identityCode.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取认证时间
     *
     * @return create_time - 认证时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置认证时间
     *
     * @param createTime 认证时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}