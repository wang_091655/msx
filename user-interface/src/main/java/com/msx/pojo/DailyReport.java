package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "tb_daily_report")
public class DailyReport implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 每日用户注册数
     */
    @Column(name = "reg_num")
    private Integer regNum;

    /**
     * 绑卡数
     */
    @Column(name = "bound_num")
    private Integer boundNum;

    /**
     * 运营商认证数
     */
    @Column(name = "operator_authentication_num")
    private Integer operatorAuthenticationNum;

    /**
     * 淘宝认证数
     */
    @Column(name = "taobao__authentication_num")
    private Integer taobaoAuthenticationNum;

    /**
     * 实名认证数
     */
    @Column(name = "identity_num")
    private Integer identityNum;

    /**
     * 审核通过总数
     */
    @Column(name = "audit_num")
    private Integer auditNum;

    /**
     * 放款总金额
     */
    @Column(name = "loan_all_money")
    private BigDecimal loanAllMoney;

    /**
     * 还款总金额
     */
    @Column(name = "repayment_all_money")
    private BigDecimal repaymentAllMoney;

    /**
     * 日期
     */
    private Date today;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取每日用户注册数
     *
     * @return reg_num - 每日用户注册数
     */
    public Integer getRegNum() {
        return regNum;
    }

    /**
     * 设置每日用户注册数
     *
     * @param regNum 每日用户注册数
     */
    public void setRegNum(Integer regNum) {
        this.regNum = regNum;
    }

    /**
     * 获取绑卡数
     *
     * @return bound_num - 绑卡数
     */
    public Integer getBoundNum() {
        return boundNum;
    }

    /**
     * 设置绑卡数
     *
     * @param boundNum 绑卡数
     */
    public void setBoundNum(Integer boundNum) {
        this.boundNum = boundNum;
    }

    /**
     * 获取运营商认证数
     *
     * @return operator_authentication_num - 运营商认证数
     */
    public Integer getOperatorAuthenticationNum() {
        return operatorAuthenticationNum;
    }

    /**
     * 设置运营商认证数
     *
     * @param operatorAuthenticationNum 运营商认证数
     */
    public void setOperatorAuthenticationNum(Integer operatorAuthenticationNum) {
        this.operatorAuthenticationNum = operatorAuthenticationNum;
    }

    /**
     * 获取淘宝认证数
     *
     * @return taobao__authentication_num - 淘宝认证数
     */
    public Integer getTaobaoAuthenticationNum() {
        return taobaoAuthenticationNum;
    }

    /**
     * 设置淘宝认证数
     *
     * @param taobaoAuthenticationNum 淘宝认证数
     */
    public void setTaobaoAuthenticationNum(Integer taobaoAuthenticationNum) {
        this.taobaoAuthenticationNum = taobaoAuthenticationNum;
    }

    /**
     * 获取实名认证数
     *
     * @return identity_num - 实名认证数
     */
    public Integer getIdentityNum() {
        return identityNum;
    }

    /**
     * 设置实名认证数
     *
     * @param identityNum 实名认证数
     */
    public void setIdentityNum(Integer identityNum) {
        this.identityNum = identityNum;
    }

    /**
     * 获取审核通过总数
     *
     * @return audit_num - 审核通过总数
     */
    public Integer getAuditNum() {
        return auditNum;
    }

    /**
     * 设置审核通过总数
     *
     * @param auditNum 审核通过总数
     */
    public void setAuditNum(Integer auditNum) {
        this.auditNum = auditNum;
    }

    /**
     * 获取放款总金额
     *
     * @return loan_all_money - 放款总金额
     */
    public BigDecimal getLoanAllMoney() {
        return loanAllMoney;
    }

    /**
     * 设置放款总金额
     *
     * @param loanAllMoney 放款总金额
     */
    public void setLoanAllMoney(BigDecimal loanAllMoney) {
        this.loanAllMoney = loanAllMoney;
    }

    /**
     * 获取还款总金额
     *
     * @return repayment_all_money - 还款总金额
     */
    public BigDecimal getRepaymentAllMoney() {
        return repaymentAllMoney;
    }

    /**
     * 设置还款总金额
     *
     * @param repaymentAllMoney 还款总金额
     */
    public void setRepaymentAllMoney(BigDecimal repaymentAllMoney) {
        this.repaymentAllMoney = repaymentAllMoney;
    }

    /**
     * 获取日期
     *
     * @return today - 日期
     */
    public Date getToday() {
        return today;
    }

    /**
     * 设置日期
     *
     * @param today 日期
     */
    public void setToday(Date today) {
        this.today = today;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}