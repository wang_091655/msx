package com.msx.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "tb_system_information")
public class SystemInformation implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 记录内容
     */
    private String content;

    /**
     * 记录时间
     */
    @Column(name = "record_time")
    private Date recordTime;

    /**
     * 状态 0:未读1:已读
     */
    private String state;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取记录内容
     *
     * @return content - 记录内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置记录内容
     *
     * @param content 记录内容
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取记录时间
     *
     * @return record_time - 记录时间
     */
    public Date getRecordTime() {
        return recordTime;
    }

    /**
     * 设置记录时间
     *
     * @param recordTime 记录时间
     */
    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    /**
     * 获取状态 0:未读1:已读
     *
     * @return state - 状态 0:未读1:已读
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态 0:未读1:已读
     *
     * @param state 状态 0:未读1:已读
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}