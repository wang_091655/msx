package com.msx.risk.service;

import com.msx.pojo.BlackList;
import com.msx.util.ResponseResult;

import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-15 20:20
 */
public interface RiskService {

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 11:06 2020/6/24
     * @Param  * @param orderId
     * @return java.lang.String
     * 根据订单号查询状态
     **/
    ResponseResult findAuditOrderStateByOrderId(String orderId);


    Object findAll();
}
