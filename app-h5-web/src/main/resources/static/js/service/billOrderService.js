//服务层
app.service('billOrderService',function($http){
	    	
	//读取列表数据绑定到表单中
	this.findAll=function(){
		return $http.get('http://localhost:8082/trade/zhuangdan');
	}
	//分页 
	this.findPage=function(page,rows){
		return $http.get('../billOrder/findPage.do?page='+page+'&rows='+rows);
	}
	//查询实体
	this.findOne=function(id){
		return $http.get('../billOrder/findOne.do?id='+id);
	}
	//增加 
	this.add=function(entity){
		return  $http.post('../billOrder/add.do',entity );
	}
	//修改 
	this.update=function(entity){
		return  $http.post('../billOrder/update.do',entity );
	}
	//删除
	this.dele=function(ids){
		return $http.get('../billOrder/delete.do?ids='+ids);
	}
	//搜索
	this.search=function(page,rows,searchEntity){
		return $http.post('../billOrder/search.do?page='+page+"&rows="+rows, searchEntity);
	}    	
});
