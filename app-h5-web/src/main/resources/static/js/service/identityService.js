//服务层
app.service('identityService',function($http){
    $http.defaults.headers.common = {'tou':localStorage.getItem("token")};
    //cro
    this.cro=function (frontImage) {
        return $http.post('http://localhost:8082/certificate/identity/orc?frontImage='+frontImage);
    }

    //身份证验证
    this.ok=function (all) {
        return $http.post('http://localhost:8082/certificate/identity?backImage='+all.identityFront+'&code='+all.code+'&frontImage='+all.identityFront+'&liveImage='+all.livingCertification+'&name='+all.name);
    }
    
    //绑卡获取初始值
    this.bindinit=function () {
        return $http.post('http://localhost:8082/certificate/identity/tiedcard/id');
    }
    //绑卡获取验证码
    this.getnumber=function (list) {
        return $http.post('http://localhost:8082/certificate/identity/tiedcard/sms?bankId='+list.bankId.id+'&bankcode='+list.bankCode+'&indentcode='+list.identityCode+'&name='+list.name+'&phone='+list.reservedMobile+'&userid=1');
    }

    //绑卡验证
    this.tiok=function (all) {
        return $http.post('http://localhost:8082/certificate/identity/tiedcard?bankId='+all.bankId.id+'&bankcode='+all.bankCode+'&code='+all.code+'&indentcode='+all.identityCode+'&name='+all.name+'&phone='+all.reservedMobile+'&userid=1');
    }


    //根据id获取信息
    this.identityGet=function () {
        return $http.get('http://localhost:8082/usershow/findidentityByUserId');
    }

});
