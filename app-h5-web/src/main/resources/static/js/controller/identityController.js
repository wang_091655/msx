//控制层
app.controller('identityController',function($scope,identityService,uploadService){

    //$controller('',{$scope:$scope});//继承
    $scope.identity={"identityFront":"","identityBack":"","livingCertification":"","name":"","code":""};
    $scope.fag={};
    $scope.s={};

    //绑卡初始值
    $scope.bindlist={};
    $scope.showlist=[];


    $scope.t={};
    $scope.tibacklist={"bankId":{},"bindCardId":""};



    //上传1
    $scope.update1= function(){
        alert("COR扫描中");
        uploadService.uploadFile().success(function (res) {
            if (res.code==20000) {
                $scope.identity.identityFront=res.msg;
                //cor认证
                identityService.cro($scope.identity.identityFront).success(function (c) {
                    console.log(c);
                         $scope.fag =JSON.parse(c.data);
                                if ($scope.fag!=null) {
                                    alert("识别成功");
                                     $scope.identity.name=$scope.fag.cardName;
                                     $scope.identity.code=$scope.fag.cardNo;
                                }else {
                                    alert("请重新上传");
                                }
                })
                //cor
            }else {
                alert("上传失败");
                location.reload();
            }
        }).error(function (res) {
            alert("失败有误");
            location.reload();
        })

    }


    //上传2
    $scope.update2= function(){
     uploadService.uploadFile().success(function (res) {
         if (res.code==20000) {
             alert("上传成功");
             $scope.identity.identityBack=res.msg;
         }else {
             alert("上传失败");
         }
     });
            
    }

    //上传3
    $scope.update3= function(){
        uploadService.uploadFile().success(function (res) {
            if (res.code==20000) {
                alert("上传成功");
                $scope.identity.livingCertification=res.msg;
            }else {
                alert("上传失败");
            }
        });

    }

    //认证
    $scope.ok= function(){
        identityService.ok($scope.identity).success(function (res) {
            alert(res.msg);
            if (res.code==20000) {
                location.href="anth_page.html";
            }
        });

    }
    //绑卡获取初始值
    $scope.bind=function () {
        identityService.bindinit().success(function (b) {
            console.log(b);
            if (b.code==20000) {
                $scope.bindlist=b.data.user;
                $scope.showlist=b.data.bankshow;
                $scope.tibacklist.bankId= $scope.showlist[0];

            }
        });
    }
    //绑卡获取验证码
    $scope.getnumber=function () {
        if ($scope.tibacklist.bankCode==null){
            alert("银行卡为空");
        }
        if ($scope.tibacklist.reservedMobile==null){
            alert("手机号为空");
        }
        if ($scope.tibacklist.reservedMobile != null && $scope.tibacklist.bankCode != null) {
            identityService.getnumber($scope.tibacklist).success(function (g) {
                console.log(g);
                alert(g.msg);
                if (g.code==20000) {
                   $scope.t= JSON.parse(g.data);

                 var  mess = confirm("验证码为"+$scope.t.code+"是否要复制验证码?");
                 if (mess){
                     $scope.tibacklist.code=$scope.t.code;
                 }
                 $scope.tibacklist.bindCardId=$scope.t.bindCardId;
                }
            });
        }

    }

    //绑卡确认
    $scope.tiok=function () {
        $scope.tibacklist.name=$scope.bindlist.name;
        $scope.tibacklist.identityCode=$scope.bindlist.identityCode;
        identityService.tiok($scope.tibacklist).success(function (tk) {
            alert(tk.msg);
            if (tk.code==20000) {
                location.href="anth_page.html";
            }
        })

    }


});
