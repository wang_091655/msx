package com.credit.app.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Title:IntelliJ IDEA
 * Copyright: Copyright(c)2020 版权
 *
 * @PackageName:com.credit.app.web
 * @ClassName:AppWebApplication
 * @Description: TODO
 * @Author 豪少
 * @Date 2020/6/16 19:49
 * @Version 1.0
 **/
@SpringBootApplication
public class AppWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppWebApplication.class, args);
    }
}
