package com.msx.risk.refactoring;

import com.msx.pojo.*;
import com.msx.risk.service.impl.RiskServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Title:IntelliJ IDEA
 * Copyright: Copyright(c)2020 版权
 *
 * @PackageName:com.msx.refactoring
 * @ClassName:AuditFactory
 * @Description: TODO
 * @Author 豪少
 * @Date 2020/6/22 20:43
 * @Version 1.0
 **/
@Slf4j
@Service
public class AuditFactory {


    @Autowired
    private RiskServiceImpl riskService;

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 20:50 2020/6/22
     * @Param  * @param null
     * @return
     * 状态码 1 已认证
     **/
    private static final String CODE = "1";

    public boolean getAudit(User user, Integer auditOrderId){
        String userAuthentication = user.getUserAuthentication();
        log.info("++++++-userAuthentication"+userAuthentication);
        boolean equals = user.getUserAuthentication().equals(CODE);
        //身份认证没通过，添加记录
        if(!user.getUserAuthentication().equals(CODE)){
            log.info("+++++身份结果"+equals);
            addRiskControlRecord("身份认证", "1", auditOrderId);
            return false;
        }
        addRiskControlRecord("身份认证","0",auditOrderId);
        //绑卡没通过
        if(!user.getLinkmanAuthentication().equals(CODE)){
            addRiskControlRecord("绑卡认证","1",auditOrderId);
            return false;
        }
        addRiskControlRecord("绑卡认证","0",auditOrderId);
        //通讯录认证没通过
        if(!user.getAddressbookAuthentication().equals(CODE)){
            addRiskControlRecord("通讯录认证","1",auditOrderId);
            return false;
        }
        addRiskControlRecord("通讯录认证","0",auditOrderId);
        //运营商认证没通过
        if(!user.getOperatorAuthentication().equals(CODE)){
            addRiskControlRecord("运营商认证","1",auditOrderId);
            return false;
        }
        addRiskControlRecord("运营商认证","0",auditOrderId);
        //淘宝认证没通过
        if(!user.getTaobaoAuthentication().equals(CODE)){
            addRiskControlRecord("淘宝认证","1",auditOrderId);
            return false;
        }
        addRiskControlRecord("淘宝认证","0",auditOrderId);
        return true;
    }

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 9:54 2020/6/23
     * @Param  * @param operator
     * @param taobao
     * @param auditOrderId
     * @return com.msx.risk.refactoring.AbstractModel
     * 淘宝运营商数据分析
     **/
    public boolean getAnalyzeData(Operator operator, Taobao taobao, Integer auditOrderId){

        if(StringUtils.isEmpty(operator.getMessage()) || StringUtils.isEmpty(taobao.getMessage())){
            //数据分析
            addRiskControlRecord("淘宝运营商数据分析","1",auditOrderId);
            return false;
        }
        //数据分析
        addRiskControlRecord("淘宝运营商数据分析","0",auditOrderId);
        return true;

    }
    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 10:17 2020/6/23
     * @Param  * @param whiteList
     * @param blackList
     * @param man
     * @param blackByUserNameOne
     * @param blackByUserNameTwo
     * @return com.msx.risk.refactoring.AbstractModel
     * 联系人认证
     **/
    public boolean getAuditBlackOrWhite(WhiteList whiteList, BlackList blackList, Linkman man, BlackList blackByUserNameOne, BlackList blackByUserNameTwo, Integer auditOrderId){
        //用户不在白名单并且不在黑名单
        if (StringUtils.isEmpty(whiteList) && StringUtils.isEmpty(blackList)) {
            log.info("=======man值========" + man);
            //判断联系人对象是否为空
            if (!StringUtils.isEmpty(man)) {
                addRiskControlRecord("用户黑白名单认证","1",auditOrderId);
                return false;
            }
            addRiskControlRecord("用户黑白名单认证","0",auditOrderId);
            //查询第一联系人，判断是否在黑名单 存在审核失败
            if (!StringUtils.isEmpty(blackByUserNameOne)) {
                addRiskControlRecord("第一联系人认证","1",auditOrderId);
                return false;
            }
            addRiskControlRecord("第一联系人认证","0",auditOrderId);
            //查询第二联系人，判断是否在黑名单 存在审核失败
            if (StringUtils.isEmpty(blackByUserNameTwo)) {
                addRiskControlRecord("第二联系人认证","0",auditOrderId);
                return false;
            }
            addRiskControlRecord("第二联系人认证","0",auditOrderId);
            return true;
        }
        //用户在白名单
        if (!StringUtils.isEmpty(whiteList)) {
            addRiskControlRecord("用户黑白名单认证","0",auditOrderId);
            return true;
        }
        return false;
    }

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 18:57 2020/6/22
     * @Param  * @param
     * @return void
     * 风控记录添加
     **/
    private void addRiskControlRecord(String auditType,String state,Integer auditOrderId){
        AuditcarefuleOrder auditcarefuleOrder = new AuditcarefuleOrder();
        auditcarefuleOrder.setAuditType(auditType);
        auditcarefuleOrder.setState(state);
        auditcarefuleOrder.setAuditOrderId(auditOrderId);
        auditcarefuleOrder.setAuditTime(new Date());
        auditcarefuleOrder.setIsDelete("0");
        riskService.addRiskOrder(auditcarefuleOrder);
    }


}
