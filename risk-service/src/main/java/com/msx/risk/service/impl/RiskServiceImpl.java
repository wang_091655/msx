package com.msx.risk.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.constants.StatusCode;
import com.msx.pojo.*;
import com.msx.risk.mapper.*;
import com.msx.user.service.UserService;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-15 20:20
 */
@Service
public class RiskServiceImpl {

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 19:17 2020/6/18
     * @Param  * @param null
     * @return
     * AuditcarefuleOrderMapper:风控记录
     * BlackListMapper: 黑名单
     * AuditOrderMapper:审核记录
     * OrderMapper:订单
     * WhiteListMapper:白名单
     * UserMapper:用户表
     **/

    @Autowired
    private BlackListMapper blackListMapper;

    @Autowired
    private AuditcarefuleOrderMapper auditcarefuleOrderMapper;

    @Autowired
    private AuditOrderMapper auditOrderMapper;

    @Autowired
    private WhiteListMapper whiteListMapper;


    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 11:01 2020/6/24
     * @Param  * @param orderId
     * @return java.lang.String
     * 根据订单号查询审核状态
     **/
    public ResponseResult findAuditOrderStateByOrderId(String orderId){
        //初始化查询条件
        Example example = new Example(AuditOrder.class);
        Example.Criteria criteria = example.createCriteria();

        //添加条件
        criteria.andEqualTo("orderId",orderId);
        AuditOrder auditOrder = auditOrderMapper.selectOneByExample(example);
        if(StringUtils.isEmpty(auditOrder)){
            return new ResponseResult(StatusCode.ERROR,"没有此订单的记录!");
        }
        return new ResponseResult(StatusCode.OK,"查询成功!",auditOrder.getState());
    }


    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 9:17 2020/6/19
     * @Param  * @param idCard
     * @return com.msx.pojo.BlackList
     * 黑名单
     **/
    public BlackList findBlackByUserName(String name){
        //初始化查询条件
        Example example = new Example(BlackList.class);
        Example.Criteria criteria = example.createCriteria();
        //添加条件
        criteria.andEqualTo("userName",name);
        return blackListMapper.selectOneByExample(example);
    }

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 9:19 2020/6/19
     * @Param  * @param idCard
     * @return com.msx.pojo.WhiteList
     * 白名单
     **/
    public WhiteList findWhiteByUserName(String name){
        //初始化查询条件
        Example example = new Example(WhiteList.class);
        Example.Criteria criteria = example.createCriteria();
        //添加条件
        criteria.andEqualTo("userName",name);
        return whiteListMapper.selectOneByExample(example);
    }

    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 19:41 2020/6/22
     * @Param  * @param auditOrder
     * @return void
     * 添加审核记录
     **/
    public void addAuditOrder(AuditOrder auditOrder){
        this.auditOrderMapper.insertSelective(auditOrder);
    }
    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 21:06 2020/6/22
     * @Param  * @param userId
     * @return com.msx.pojo.AuditOrder
     * 根据用户id查询审核表主键
     **/
    public AuditOrder findAuditOrderByUserId(Integer userId){
        //初始化查询条件
        Example example = new Example(AuditOrder.class);
        Example.Criteria criteria = example.createCriteria();
        //添加条件
        criteria.andEqualTo("userId",userId);
        return auditOrderMapper.selectOneByExample(example);
    }
    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 9:34 2020/6/24
     * @Param  * @param state
     * @return java.util.List<com.msx.pojo.AuditOrder>
     * 根据状态查询审核表记录
     **/
    public List<AuditOrder> findAuditOrderByState(String state){
        //初始化查询条件
        Example example = new Example(AuditOrder.class);
        Example.Criteria criteria = example.createCriteria();
        //添加条件
        criteria.andEqualTo("state",state);
        return auditOrderMapper.selectByExample(example);
    }
    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 19:42 2020/6/22
     * @Param  * @param auditOrder
     * @return void
     * 修改审核记录
     **/
    public void updateAuditOrder(AuditOrder auditOrder){
        this.auditOrderMapper.updateByPrimaryKeySelective(auditOrder);
    }

    public void addRiskOrder(AuditcarefuleOrder auditcarefuleOrder){
        this.auditcarefuleOrderMapper.insertSelective(auditcarefuleOrder);
    }
}
