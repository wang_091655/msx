package com.msx.risk.mapper;

import com.msx.pojo.AuditcarefuleOrder;
import com.msx.risk.base.IBaseMapper;

public interface AuditcarefuleOrderMapper extends IBaseMapper<AuditcarefuleOrder> {
}