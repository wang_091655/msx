package com.msx.risk.mapper;

import com.msx.pojo.WhiteList;
import com.msx.risk.base.IBaseMapper;

public interface WhiteListMapper extends IBaseMapper<WhiteList> {
}