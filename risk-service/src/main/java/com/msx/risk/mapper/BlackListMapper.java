package com.msx.risk.mapper;

import com.msx.pojo.BlackList;
import com.msx.risk.base.IBaseMapper;

public interface BlackListMapper extends IBaseMapper<BlackList> {
}