package com.msx.risk.mapper;

import com.msx.pojo.AuditOrder;
import com.msx.risk.base.IBaseMapper;

public interface AuditOrderMapper extends IBaseMapper<AuditOrder> {
}