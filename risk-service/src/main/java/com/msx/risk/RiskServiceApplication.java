package com.msx.risk;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author msx
 * @create: 2020-06-13 16:45
 */
@SpringBootApplication
@EnableDubbo
@EnableScheduling
@MapperScan("com.msx.risk.mapper")
public class RiskServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(RiskServiceApplication.class,args);
    }
}
