package com.msx.risk.config;

import com.alibaba.dubbo.config.annotation.Reference;
import com.msx.pojo.AuditOrder;
import com.msx.risk.service.impl.RiskServiceImpl;
import com.msx.util.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Title:IntelliJ IDEA
 * Copyright: Copyright(c)2020 版权
 *
 * @PackageName:com.msx.risk
 * @ClassName:ScheduledTasks
 * @Description: TODO
 * @Author 豪少
 * @Date 2020/6/18 19:30
 * @Version 1.0
 **/
@Component
@Slf4j
@RabbitListener(queues = "order")
public class ScheduledTasks {


    @Autowired
    private RiskServiceImpl riskService;


    /**
     * @Author 豪少
     * @Description //TODO
     * @Date 19:31 2020/6/18
     * @Param  * @param null
     * @return
     * 监听器
     **/

    @RabbitHandler
    public ResponseResult listenerAuditOrder(Map map){
        String orderNumber = (String) map.get("orderNumber");
        Integer userId = (Integer) map.get("userId");
        String userName = (String) map.get("userName");
        log.info("订单号"+orderNumber);
        log.info("用户id"+userId);
        log.info("用户名"+userName);
        if(StringUtils.isEmpty(orderNumber) || StringUtils.isEmpty(userId) || StringUtils.isEmpty(userName)){
            log.error("未监听到数据!");
            return ResponseResult.error();
        }
        //添加审核记录表
        AuditOrder auditOrder = new AuditOrder();
        auditOrder.setState("2");
        auditOrder.setScore(0);
        auditOrder.setOrderId(orderNumber);
        auditOrder.setUserId(userId);
        auditOrder.setAuditTime(new Date());
        auditOrder.setIsDelete("0");
        this.riskService.addAuditOrder(auditOrder);
        return ResponseResult.success();

    }




}
