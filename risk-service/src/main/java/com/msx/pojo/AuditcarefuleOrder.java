package com.msx.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_auditcarefule_order")
public class AuditcarefuleOrder implements Serializable {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 审核类型
     */
    @Column(name = "audit_type")
    private String auditType;

    /**
     * 审核状态 0:审核通过1:审核未通过
     */
    private String state;

    /**
     * 审核记录id
     */
    @Column(name = "audit_order_id")
    private Integer auditOrderId;

    /**
     * 审核时间
     */
    @Column(name = "audit_time")
    private Date auditTime;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取审核类型
     *
     * @return audit_type - 审核类型
     */
    public String getAuditType() {
        return auditType;
    }

    /**
     * 设置审核类型
     *
     * @param auditType 审核类型
     */
    public void setAuditType(String auditType) {
        this.auditType = auditType == null ? null : auditType.trim();
    }

    /**
     * 获取审核状态 0:审核通过1:审核未通过
     *
     * @return state - 审核状态 0:审核通过1:审核未通过
     */
    public String getState() {
        return state;
    }

    /**
     * 设置审核状态 0:审核通过1:审核未通过
     *
     * @param state 审核状态 0:审核通过1:审核未通过
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * 获取审核记录id
     *
     * @return audit_order_id - 审核记录id
     */
    public Integer getAuditOrderId() {
        return auditOrderId;
    }

    /**
     * 设置审核记录id
     *
     * @param auditOrderId 审核记录id
     */
    public void setAuditOrderId(Integer auditOrderId) {
        this.auditOrderId = auditOrderId;
    }

    /**
     * 获取审核时间
     *
     * @return audit_time - 审核时间
     */
    public Date getAuditTime() {
        return auditTime;
    }

    /**
     * 设置审核时间
     *
     * @param auditTime 审核时间
     */
    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}