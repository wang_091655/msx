package com.msx.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_tiedcard")
public class Tiedcard implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 身份证号
     */
    @Column(name = "identity_code")
    private String identityCode;

    /**
     * 银行id
     */
    @Column(name = "bank_id")
    private Integer bankId;

    /**
     * 银行卡号
     */
    @Column(name = "bank_code")
    private String bankCode;

    /**
     * 银行预留手机号
     */
    @Column(name = "reserved_mobile")
    private String reservedMobile;

    /**
     * 绑卡成功协议号
     */
    @Column(name = "bindCardId")
    private String bindcardid;

    /**
     * 绑卡验证码
     */
    private String code;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 绑卡时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取身份证号
     *
     * @return identity_code - 身份证号
     */
    public String getIdentityCode() {
        return identityCode;
    }

    /**
     * 设置身份证号
     *
     * @param identityCode 身份证号
     */
    public void setIdentityCode(String identityCode) {
        this.identityCode = identityCode == null ? null : identityCode.trim();
    }

    /**
     * 获取银行id
     *
     * @return bank_id - 银行id
     */
    public Integer getBankId() {
        return bankId;
    }

    /**
     * 设置银行id
     *
     * @param bankId 银行id
     */
    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    /**
     * 获取银行卡号
     *
     * @return bank_code - 银行卡号
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * 设置银行卡号
     *
     * @param bankCode 银行卡号
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode == null ? null : bankCode.trim();
    }

    /**
     * 获取银行预留手机号
     *
     * @return reserved_mobile - 银行预留手机号
     */
    public String getReservedMobile() {
        return reservedMobile;
    }

    /**
     * 设置银行预留手机号
     *
     * @param reservedMobile 银行预留手机号
     */
    public void setReservedMobile(String reservedMobile) {
        this.reservedMobile = reservedMobile == null ? null : reservedMobile.trim();
    }

    /**
     * 获取绑卡成功协议号
     *
     * @return bindCardId - 绑卡成功协议号
     */
    public String getBindcardid() {
        return bindcardid;
    }

    /**
     * 设置绑卡成功协议号
     *
     * @param bindcardid 绑卡成功协议号
     */
    public void setBindcardid(String bindcardid) {
        this.bindcardid = bindcardid == null ? null : bindcardid.trim();
    }

    /**
     * 获取绑卡验证码
     *
     * @return code - 绑卡验证码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置绑卡验证码
     *
     * @param code 绑卡验证码
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取绑卡时间
     *
     * @return create_time - 绑卡时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置绑卡时间
     *
     * @param createTime 绑卡时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}