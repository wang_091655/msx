package com.msx.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_taobao")
public class Taobao implements Serializable {
    /**
     * ID
     */
    @Id
    private Integer id;

    /**
     * 淘宝账号
     */
    @Column(name = "taobao_user")
    private String taobaoUser;

    /**
     * 淘宝密码
     */
    @Column(name = "taobao_pwd")
    private String taobaoPwd;

    /**
     * 信息
     */
    private String message;

    /**
     * 认证时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 逻辑删除 0:未删除1:删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取淘宝账号
     *
     * @return taobao_user - 淘宝账号
     */
    public String getTaobaoUser() {
        return taobaoUser;
    }

    /**
     * 设置淘宝账号
     *
     * @param taobaoUser 淘宝账号
     */
    public void setTaobaoUser(String taobaoUser) {
        this.taobaoUser = taobaoUser == null ? null : taobaoUser.trim();
    }

    /**
     * 获取淘宝密码
     *
     * @return taobao_pwd - 淘宝密码
     */
    public String getTaobaoPwd() {
        return taobaoPwd;
    }

    /**
     * 设置淘宝密码
     *
     * @param taobaoPwd 淘宝密码
     */
    public void setTaobaoPwd(String taobaoPwd) {
        this.taobaoPwd = taobaoPwd == null ? null : taobaoPwd.trim();
    }

    /**
     * 获取信息
     *
     * @return message - 信息
     */
    public String getMessage() {
        return message;
    }

    /**
     * 设置信息
     *
     * @param message 信息
     */
    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    /**
     * 获取认证时间
     *
     * @return create_time - 认证时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置认证时间
     *
     * @param createTime 认证时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取逻辑删除 0:未删除1:删除
     *
     * @return is_delete - 逻辑删除 0:未删除1:删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置逻辑删除 0:未删除1:删除
     *
     * @param isDelete 逻辑删除 0:未删除1:删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete == null ? null : isDelete.trim();
    }
}