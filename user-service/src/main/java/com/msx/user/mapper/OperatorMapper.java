package com.msx.user.mapper;

import com.msx.pojo.Operator;
import com.msx.user.base.IBaseMapper;

public interface OperatorMapper extends IBaseMapper<Operator> {
}