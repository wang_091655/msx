package com.msx.user.mapper;

import com.msx.pojo.DailyReport;
import com.msx.user.base.IBaseMapper;

public interface DailyReportMapper extends IBaseMapper<DailyReport> {
}