package com.msx.user.mapper;

import com.msx.pojo.Linkman;
import com.msx.user.base.IBaseMapper;

public interface LinkmanMapper extends IBaseMapper<Linkman> {
}