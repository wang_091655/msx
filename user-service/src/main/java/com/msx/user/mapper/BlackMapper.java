package com.msx.user.mapper;

import com.msx.pojo.Black;
import com.msx.user.base.IBaseMapper;

public interface BlackMapper extends IBaseMapper<Black> {
}