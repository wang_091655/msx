package com.msx.user.mapper;

import com.msx.pojo.SystemInformation;
import com.msx.user.base.IBaseMapper;

public interface SystemInformationMapper extends IBaseMapper<SystemInformation> {
}