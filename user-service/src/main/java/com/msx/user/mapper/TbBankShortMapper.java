package com.msx.user.mapper;

import com.msx.pojo.TbBankShort;
import com.msx.user.base.IBaseMapper;

public interface TbBankShortMapper extends IBaseMapper<TbBankShort> {
}