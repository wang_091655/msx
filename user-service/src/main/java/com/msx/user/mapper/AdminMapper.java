package com.msx.user.mapper;

import com.msx.pojo.Admin;
import com.msx.user.base.IBaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AdminMapper extends IBaseMapper<Admin> {

    @Select("select * from tb_admin")
    public List<Admin> getAdminList();
}