package com.msx.user.mapper;

import com.msx.pojo.User;
import com.msx.user.base.IBaseMapper;

public interface UserMapper extends IBaseMapper<User> {
}