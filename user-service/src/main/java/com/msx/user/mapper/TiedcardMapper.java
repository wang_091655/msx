package com.msx.user.mapper;

import com.msx.pojo.Tiedcard;
import com.msx.user.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface TiedcardMapper extends IBaseMapper<Tiedcard> {
    @Select("SELECT * from tb_tiedcard where user_id=#{userid}")
    public Tiedcard findByIduserid(Integer userid);
}