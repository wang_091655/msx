package com.msx.user.mapper;

import com.msx.pojo.Sms;
import com.msx.user.base.IBaseMapper;

public interface SmsMapper extends IBaseMapper<Sms> {
}