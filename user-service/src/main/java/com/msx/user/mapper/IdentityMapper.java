package com.msx.user.mapper;

import com.msx.pojo.Identity;
import com.msx.user.base.IBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface IdentityMapper extends IBaseMapper<Identity> {
    @Select(" SELECT * from tb_identity where user_id=#{userid}")
    Identity selectidentity(Integer userid);
}