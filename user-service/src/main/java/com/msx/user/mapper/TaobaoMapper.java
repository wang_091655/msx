package com.msx.user.mapper;

import com.msx.pojo.Taobao;
import com.msx.user.base.IBaseMapper;

public interface TaobaoMapper extends IBaseMapper<Taobao> {
}