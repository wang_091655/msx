package com.msx.user.mapper;


import com.msx.pojo.AddressBook;
import com.msx.user.base.IBaseMapper;

public interface AddressBookMapper extends IBaseMapper<AddressBook> {
}