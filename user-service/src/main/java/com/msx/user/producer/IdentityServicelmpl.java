package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.Identity;
import com.msx.pojo.User;
import com.msx.user.service.IdentityServiceInterface;
import com.msx.user.service.impl.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;

@Service(timeout = 6000,retries=0,version = "2.0.0")
public class IdentityServicelmpl implements IdentityServiceInterface {

    @Autowired
    private IdentityService service;

    @Override
    public String UserUpdate(User user) {
        service.UserUpdate(user);
        return "ok";
    }

    @Override
    public String identityAuth(String name, String cardId, String base64FrontPic) {
        return service.identityAuth(name, cardId, base64FrontPic);
    }

    @Override
    public String identitybank(String name, String cardId, String base64FrontPic) {
        return null;
    }

    @Override
    public String identityOrc(String name, String cardId, String base64FrontPic) {
        return service.identityOrc(base64FrontPic);
    }

    @Override
    public String liveAuth(String name, String cardId, String base64FrontPic) {
        return service.liveAuth(base64FrontPic);
    }

    @Override
    public Identity selectidentity(Integer userid) {
        return service.selectidentity(userid);
    }
}
