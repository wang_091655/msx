package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.msx.user.service.UserIdentityHomeInterface;
import com.msx.user.service.impl.IdentityService;
import com.msx.user.service.impl.UserIdentityHome;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Service(timeout = 6000,retries=0,version = "2.0.0")
@Log4j2
public class UserIdentityHomeimpl implements UserIdentityHomeInterface {
    @Autowired
    private UserIdentityHome userIdentityHome;

    @Autowired
    private IdentityService service;

    @Override
    public ResponseResult UserIdentityHome(String id, String name, String identitycode, String frontImage, String backImage, String liveImage) {
       log.info(identitycode);
        return userIdentityHome.UserIdentityHome(id, name, identitycode, frontImage, backImage, liveImage);
    }

    @Override
    public ResponseResult identityOrc(String base64FrontPic) {
        String identityOrc = service.identityOrc(base64FrontPic);
        String data = JSON.parseObject(identityOrc).getString("data");
        System.out.println(identityOrc);
        if (!StringUtils.isBlank(identityOrc)){
            return new ResponseResult().success(data);
        }
        return new ResponseResult().error(StatusCode.ERROR,"Orc认证失败");
    }

    @Override
    public ResponseResult identityEcho(String useid) {
        return userIdentityHome.findById(useid);
    }
}
