package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.Taobao;
import com.msx.user.service.IdentityTaoBaoServiceInterface;
import com.msx.user.service.impl.IdentityTaoBaoService;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

@Service(timeout = 6000,retries=0,version = "2.0.0")
public class IdentityTaoBaoServicelmpl implements IdentityTaoBaoServiceInterface {
    @Autowired
    private IdentityTaoBaoService identityTaoBaoService;


    @Override
    public ResponseResult IdentityTaoBaoService(Taobao taobao) {
        return identityTaoBaoService.IdentityTaoBao(taobao);
    }

    @Override
    public ResponseResult findTaobaoByUserId(Integer userId) {
        return identityTaoBaoService.findTaobaoByUserId(userId);
    }
}
