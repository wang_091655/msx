package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.Admin;
import com.msx.pojo.User;
import com.msx.user.service.UserProducerInterface;
import com.msx.user.service.impl.UserServiceImpl;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author dutao
 * @create: 2020-06-16 10:19
 */
@Service(timeout = 6000,retries=0,version = "2.0.0")
public class UserProducerImpl implements UserProducerInterface {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public List<Admin> findAll() {
        return userService.findAll();
    }

    @Override
    public List<User> findUser() {
        return userService.findUser();
    }

    //手机 密码 登录
    @Override
    public ResponseResult login(User user) {
        return userService.login(user);
    }

    //注册 验证码
    @Override
    public ResponseResult register(User user, String code) {
        return userService.register(user,code);
    }

    @Override
    public User findById(Integer id) {
        return userService.findById(id);
    }

    //手机 验证码 登录
    @Override
    public ResponseResult codeLogin(User user, String code) {
        return userService.codeLogin(user,code);
    }

    //忘记密码
    @Override
    public ResponseResult forget(User user, String code) {
        return userService.forget(user,code);
    }

    //获取验证码
    @Override
    public ResponseResult codeSms(String mobile) {
        return userService.codeSms(mobile);
    }


}
