package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.user.service.AddressbookServiceInterface;
import com.msx.user.service.impl.AddressbookService;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;

@Service(timeout = 6000,retries=0,version = "2.0.0")
public class AddressbookServicelmpl implements AddressbookServiceInterface {

    @Autowired
    private AddressbookService addressbookService;

    @Override
    public ResponseResult update(String id) {
        try {
            addressbookService.update(id);
            return new ResponseResult(StatusCode.OK,"通讯录认证成功",null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult().error(StatusCode.ERROR,"认证失败");
    }
}
