package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.Linkman;
import com.msx.user.service.LinkManServiceInterface;
import com.msx.user.service.impl.LinkManService;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

@Service(timeout = 6000,retries=0,version = "2.0.0")
public class LinkManServiceImpl implements LinkManServiceInterface {
    @Autowired
    private LinkManService tbLinkManService;

    @Override
    public ResponseResult identificationTbLinkMan(Linkman tbLinkman) {
        System.out.println(tbLinkman);
      return   tbLinkManService.tbLinkmanAdd(tbLinkman);
    }

    /**
     * 根据地点查询信息
     * @param userId
     * @return
     */
    @Override
    public ResponseResult findLinkManByUserId(Integer userId) {
        return tbLinkManService.findLinkManByUserId(userId);
    }
}
