package com.msx.user.producer;

import com.alibaba.dubbo.config.annotation.Service;
import com.msx.pojo.Tiedcard;
import com.msx.user.service.TiedcardServiceInterface;
import com.msx.user.service.impl.Smslmpl;
import com.msx.user.service.impl.TiedcardService;
import com.msx.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 发送验证码
 * 绑定验证码
 */
@Service(timeout = 6000,retries=0,version = "2.0.0")
public class TiedcardServicelimpl  implements TiedcardServiceInterface {
    @Autowired
    private TiedcardService tiedcardService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private Smslmpl smslmpl;

    /**
     * 发送信息
     * @param tiedcard
     * @return
     */
    @Override
    public ResponseResult TiedcardOut(Tiedcard tiedcard) {

        return tiedcardService.TiedcardOut(tiedcard);
    }

    /**
     * 绑卡认证
     * @param tiedcard
     * @return
     */
    @Override
    public ResponseResult indeupdate(Tiedcard tiedcard) {
        return tiedcardService.indeupdate(tiedcard);
    }

    /**
     * 查询信息
     * @param id
     * @return
     */
    @Override
    public ResponseResult findById(String id) {
        return tiedcardService.findByID(id);
    }

    @Override
    public Tiedcard findByIduserid(Integer userid) {
        return tiedcardService.findByIduserid(userid);
    }
}
