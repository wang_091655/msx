package com.msx.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.msx.constants.IsDeleteEnum;
import com.msx.pojo.Taobao;
import com.msx.pojo.User;
import com.msx.user.mapper.TaobaoMapper;
import com.msx.user.mapper.UserMapper;
import com.msx.util.HttpClientUtils;
import com.msx.util.MD5Sign;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 淘宝认证后台
 */
@Service
public class IdentityTaoBaoService {

        @Value("${user.cardId}")
        private String CardId;
        @Value("${user.appSercret}")
        private String appSercret;

        @Autowired
        private TaobaoMapper taobaoMapper;
        @Autowired
        private UserMapper userMapper;


    /**
     * 根据用户id查询淘宝表
     * @param userId
     * @return
     */
    public ResponseResult findTaobaoByUserId(Integer userId){

        Example example = new Example(Taobao.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId",userId);
        List<Taobao> taobaos = taobaoMapper.selectByExample(example);

        if (taobaos.size()==0){
            return new ResponseResult().error(StatusCode.ERROR,"抱歉没有该用户信息");
        }

        return new ResponseResult().success(taobaos.get(taobaos.size()-1));
    }



    /**
     * 认证
     * 添加认证信息
     * 改变认证状态
     * @return
     */
    @Transactional
    public ResponseResult IdentityTaoBao(Taobao taobao){
        //认证
        String falst = this.IdentityTT(taobao);
        JSONObject json = JSON.parseObject(falst);
        String message = json.getString("code");
        String s = json.toString();

        // 添加认证信息
        if (String.valueOf(IsDeleteEnum.OK.getCode()).equals(message)){
            taobao.setMessage(s);
            taobao.setIsDelete(String.valueOf(IsDeleteEnum.OK.getCode()));
            taobao.setCreateTime(new Date());
            taobaoMapper.insertSelective(taobao);
            //改变认证状态
            User user = new User();
            user.setId(taobao.getUserId());
            user.setTaobaoAuthentication("1");
            userMapper.updateByPrimaryKeySelective(user);

           return new ResponseResult(StatusCode.OK,"淘宝认证成功",s);
        }
        return new ResponseResult(StatusCode.ERROR,"淘宝认证失败");
    }
    //认证
    public String IdentityTT(Taobao taobao){
        String url="http://127.0.0.1:8080/api/common/taobaoAuth";
        Map<String,String> map = new HashMap<>();
        map.put("merId",CardId);
        map.put("userName",taobao.getTaobaoUser());
        map.put("password",taobao.getTaobaoPwd());
        map.put("sign", MD5Sign.sign(map,appSercret));
        String post = HttpClientUtils.httpPost(url, map);
        return post;
    }
}
