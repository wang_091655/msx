package com.msx.user.service.impl;

import com.msx.constants.IsDeleteEnum;
import com.msx.pojo.Linkman;
import com.msx.pojo.User;
import com.msx.user.mapper.LinkmanMapper;
import com.msx.user.mapper.UserMapper;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class LinkManService {
    //联系人
    @Autowired
    private LinkmanMapper tbLinkmanMapper;
    //用户
    @Autowired
    private UserMapper msxUserMapper;

    /**
     * 根据用户id查询联系人表
     * @param userId
     * @return
     */
    public ResponseResult findLinkManByUserId(Integer userId){

        Example example = new Example(Linkman.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId",userId);
        List<Linkman> linkmen = tbLinkmanMapper.selectByExample(example);

        if (linkmen.size()==0){
            return new ResponseResult().error(StatusCode.ERROR,"抱歉没有该用户信息");
        }
        return new ResponseResult().success(linkmen.get(linkmen.size()-1));
    }



    /**
     * 添加联系人
     * 修改用户联系人认证状态
     * @param tbLinkman
     */
    @Transactional
    public ResponseResult tbLinkmanAdd(Linkman tbLinkman){

        // 添加联系人
        tbLinkman.setCreateTime(new Date());
        tbLinkman.setIsDelete(String.valueOf(IsDeleteEnum.OK.getCode()));
        tbLinkmanMapper.insertSelective(tbLinkman);

        //修改用户联系人认证状态
        User msxUser = new User();
        msxUser.setId(tbLinkman.getUserId());
        msxUser.setLinkmanAuthentication(String.valueOf(IsDeleteEnum.ERROR.getCode()));
        msxUserMapper.updateByPrimaryKeySelective(msxUser);
        //认证成功
        return new ResponseResult().success();
    }
}
