package com.msx.user.service.impl;

import com.msx.constants.IsDeleteEnum;
import com.msx.pojo.AddressBook;
import com.msx.pojo.User;
import com.msx.user.mapper.AddressBookMapper;
import com.msx.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 联系人认证
 */
@Service
public class AddressbookService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AddressBookMapper addressBookMapper;

    /**
     * 修改
     * @param id
     */
    @Transactional
    public void update(String id){
        User user = new User();
        user.setId(Integer.valueOf(id));
        user.setAddressbookAuthentication(String.valueOf(IsDeleteEnum.ERROR.getCode()));
        userMapper.updateByPrimaryKeySelective(user);

        //添加信息
        setAddressBookMapper(id);
    }

    //添加信息
    public void setAddressBookMapper(String id){
        List<AddressBook> bookList = new ArrayList<>();
        for (int i=1;i<10;i++ ){
            AddressBook addressBook = new AddressBook();
            addressBook.setCreateTime(new Date());
            addressBook.setIsDelete(String.valueOf(IsDeleteEnum.OK.getCode()));
            addressBook.setMobile("18515661866");
            addressBook.setName("大华"+i);
            addressBook.setUserId(Long.valueOf(id));
            addressBook.setUid(Long.valueOf(id));

            //addressBookMapper.insertSelective(addressBook);
            bookList.add(addressBook);

        }
        addressBookMapper.insertList(bookList);
    }
}
