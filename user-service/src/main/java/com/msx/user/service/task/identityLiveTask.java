package com.msx.user.service.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.msx.user.service.impl.IdentityService;

import java.util.concurrent.Callable;

/**
 * 活体安全认证
 * Callable 可以有返回状态
 */
public class identityLiveTask implements Callable<Boolean> {

    /**
     * String cardId, String base64FrontPic
     * @return
     * @throws Exception
     */

    private String livePic;
    private IdentityService identityService;

    //类构造器
    public identityLiveTask(String livePic, IdentityService identityService) {
        this.livePic = livePic;
        this.identityService = identityService;
    }

    //实现验证
    @Override
    public Boolean call() throws Exception {
        //哨兵
        Boolean flag = false;
        //启动认证
        String auth = identityService.liveAuth(livePic);
        //判断结果
        JSONObject jsonObject = JSON.parseObject(auth);
        Integer code = jsonObject.getInteger("code");
        if (code==0){
            JSONObject data = jsonObject.getJSONObject("data");
            Integer score = data.getInteger("score");
            if(score>75){
                flag=true;
            }
        }

        return flag;
    }
}
