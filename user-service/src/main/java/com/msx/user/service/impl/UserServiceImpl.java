package com.msx.user.service.impl;

import com.msx.constants.StatusCode;
import com.msx.pojo.Admin;
import com.msx.pojo.User;
import com.msx.user.mapper.AdminMapper;
import com.msx.user.mapper.UserMapper;
import com.msx.user.utils.MD5Sign;
import com.msx.util.ResponseResult;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author msx
 * @create: 2020-06-13 16:46
 */
@Service
@Log4j2
public class UserServiceImpl {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    public List<Admin> findAll() {
        return adminMapper.getAdminList();
    }
    public List<User> findUser(){
        return userMapper.selectAll();
    }

    //手机 密码 登录
    public ResponseResult login(User user) {
        log.info("*****************************");
        log.info("login"+user);
        String s = MD5Sign.md5Encode(user.getPassword());
        user.setPassword(s);
        //判断手机号
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("mobile",user.getMobile());
        /*criteria.andEqualTo("password",user.getPassword());*/
        List<User> users = userMapper.selectByExample(example);
        log.info("登录信息"+users);
        //判断密码
        Example example2 = new Example(User.class);
        Example.Criteria criteria2 = example2.createCriteria();
        /*criteria.andEqualTo("mobile",user.getMobile());*/
        criteria2.andEqualTo("password",user.getPassword());
        List<User> users2 = userMapper.selectByExample(example2);
        if (users.size()==0){
            return new ResponseResult(StatusCode.ERROR,"手机号错误");
        }
        if (users2.size()==0){
            return new ResponseResult(StatusCode.ERROR,"密码错误");
        }
        if(users!=null && users.size()>0 && users2!=null && users2.size()>0){
            UUID uuid = UUID.randomUUID();
            redisTemplate.opsForValue().set(uuid.toString(),users.get(0).getId(),15,TimeUnit.HOURS);
            System.out.println(redisTemplate.opsForValue().get(uuid.toString()));
            return new ResponseResult(StatusCode.OK,"登录成功",uuid.toString());
        }
        if (users==null && users.size()==0 && users2==null && users2.size()==0){
            return new ResponseResult(StatusCode.ERROR,"手机号，密码错误");
        }
        return new ResponseResult(StatusCode.ERROR,"登录失败");
    }
    //注册 验证码
    public ResponseResult register(User user, String code) {
        String codesms = (String) redisTemplate.opsForValue().get("codesms"+user.getMobile());
        if (StringUtils.isBlank(user.getMobile()) && StringUtils.isBlank(code) && StringUtils.isBlank(user.getPassword())){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号，验证码，密码");
        }
        if (StringUtils.isBlank(user.getMobile())){
            return ResponseResult.error("手机号不能为空");
        }
        if (StringUtils.isBlank(code)){
            return  ResponseResult.error("验证码为空");
        }
        if (StringUtils.isBlank(user.getPassword())){
            return  new ResponseResult(StatusCode.ERROR,"请输入密码");
        }
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("mobile",user.getMobile());
        /*criteria.andEqualTo("password",user.getPassword());*/
        List<User> users = userMapper.selectByExample(example);
        if (users.size()!=0){
            return new ResponseResult(StatusCode.ERROR,"用户已被注册，请换用户");
        }
        if (codesms == null){
            return new ResponseResult(StatusCode.ERROR,"验证码失效");
        }
        if(codesms != null){
            if(codesms.equals(code)){
                /*User user1 = new User();*/
                user.setCreateaTime(new Date());
                user.setUpdateTime(new Date());
                user.setMobile(user.getMobile());
                String s = MD5Sign.md5Encode(user.getPassword());
                user.setPassword(s);
                userMapper.insertSelective(user);
                return new ResponseResult(StatusCode.OK,"注册成功");
            }
            return new ResponseResult(StatusCode.ERROR,"验证码错误");
        }
        return new ResponseResult(StatusCode.ERROR,"验证码有误");
    }
    //手机 验证码 登录
    public ResponseResult codeLogin(User user, String code) {
        System.out.println("实现类"+"++++++++++++++++++++++++");
        String codes1 = (String) redisTemplate.opsForValue().get("codesms" + user.getMobile());
            if (codes1 == null){
                return new ResponseResult(StatusCode.ERROR,"验证码失效");
            }
            if (codes1!=null){
                if (codes1.equals(code)){
                    Example example = new Example(User.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("mobile",user.getMobile());
                    List<User> users = userMapper.selectByExample(example);
                    if (users.size()==0){
                        return new ResponseResult(StatusCode.ERROR,"没有此手机号,请注册");
                    }
                    if(users!=null && users.size()>0){
                        UUID uuid = UUID.randomUUID();
                        redisTemplate.opsForValue().set(uuid.toString(),users.get(0).getId(),15,TimeUnit.HOURS);
                        System.out.println("成功"+"-------------------------------");
                        return new ResponseResult(StatusCode.OK,"登录成功",uuid.toString());
                    }
                }
                return new ResponseResult(StatusCode.ERROR,"验证码错误");
            }


        return new ResponseResult(StatusCode.ERROR,"验证码错误");
    }
    //忘记密码
    public ResponseResult forget(User user, String code) {
        String codesms = (String) redisTemplate.opsForValue().get("codesms"+user.getMobile());
        if (StringUtils.isBlank(user.getMobile()) && StringUtils.isBlank(code) && StringUtils.isBlank(user.getPassword())){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号，验证码，密码");
        }
        if (StringUtils.isBlank(code)){
            return new ResponseResult(StatusCode.ERROR,"请输入验证码");
        }
        if(StringUtils.isBlank(user.getMobile())){
            return new ResponseResult(StatusCode.ERROR,"请输入手机号");
        }
        if (StringUtils.isBlank(user.getPassword())){
            return new ResponseResult(StatusCode.ERROR,"密码不能为空");
        }
        if (codesms == null){
            return new ResponseResult(StatusCode.ERROR,"验证码失效");
        }
        if (codesms!=null){
            if (codesms.equals(code)){
                Example example = new Example(User.class);
                Example.Criteria criteria = example.createCriteria();
                criteria.andEqualTo("mobile",user.getMobile());
                List<User> users = userMapper.selectByExample(example);
                System.out.println(users.get(0).getMobile()+"-----------");

                if (users.size()==0){
                    return new ResponseResult(StatusCode.ERROR,"没有该用户请注册");
                }
                User user1 = users.get(0);
                user.setId(user1.getId());
                String s = MD5Sign.md5Encode(user.getPassword());
                user.setPassword(s);
                userMapper.updateByPrimaryKeySelective(user);
                return new ResponseResult(StatusCode.OK,"修改密码成功,请重新登录");
            }
            return new ResponseResult(StatusCode.ERROR,"验证码错误");
        }
        return new ResponseResult(StatusCode.ERROR,"验证码错误");
    }
    public User findById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    //获取验证码
    public ResponseResult codeSms(String mobile) {

        if (StringUtils.isBlank(mobile)){
            return new ResponseResult().error("请输入手机号");
        }

        String o = (String) redisTemplate.opsForValue().get("codesms" + mobile);
        if (StringUtils.isBlank(o)){
            Random random = new Random();
            int code = random.nextInt(999999);
            redisTemplate.opsForValue().set("codesms"+mobile,code+"",1,TimeUnit.MINUTES);
            // String o = (String) redisTemplate.opsForValue().get("codesms" + mobile);
            //System.out.println("====================="+(String) redisTemplate.opsForValue().get("codesms" + mobile));
            return ResponseResult.success(code);
        }

        return new ResponseResult().error("请稍后获取!!!");


    }
}
