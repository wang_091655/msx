package com.msx.user.service.impl;

import com.msx.constants.IsDeleteEnum;
import com.msx.pojo.Identity;
import com.msx.pojo.User;
import com.msx.user.mapper.IdentityMapper;
import com.msx.user.mapper.UserMapper;
import com.msx.util.HttpClientUtils;
import com.msx.util.MD5Sign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 身份证认证
 * 正面
 * 反面
 * 活体
 */
@Service
public class IdentityService {

    @Value("${user.cardId}")
    private String CardId;
    @Value("${user.appSercret}")
    private String appSercret;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IdentityMapper identityMapper;

    //提交身份认证
    public void UserUpdate(User user){
        User nea = new User();
        nea.setId(user.getId());
        nea.setUserAuthentication(String.valueOf(IsDeleteEnum.ERROR.getCode()));
        userMapper.updateByPrimaryKeySelective(nea);
    }
    //ocr
    public String identityOrc(String base64FrontPic) {
        String url="http://127.0.0.1:8080/api/common/ocr";
        Map<String,String> map = new HashMap<>();
        map.put("merId",CardId);
        map.put("imagePic",base64FrontPic);
        String sign = MD5Sign.sign(map,appSercret);
        map.put("sign",sign);
        String str = HttpClientUtils.httpPost(url,map);
        return str;
    }
    //正面验证 实名认证
    public String identityAuth(String name, String cardId, String base64FrontPic) {
        String url="http://127.0.0.1:8080/api/common/identityAuth";
        Map<String,String> map = new HashMap<>();
        map.put("merId",CardId);
        map.put("name",name);
        map.put("cardId",cardId);
        map.put("frontPic",base64FrontPic);
        String sign = MD5Sign.sign(map,appSercret);
        map.put("sign",sign);
        String str = HttpClientUtils.httpPost(url,map);
        return str;
    }

    //活题认证
    public String liveAuth(String base64LivePic){
        String url="http://127.0.0.1:8080/api/common/liveAuth";
        Map<String,String> map = new HashMap<>();
        map.put("merId",CardId);
        map.put("livePic",base64LivePic);
        String sign = MD5Sign.sign(map,appSercret);
        map.put("sign",sign);
        String str = HttpClientUtils.httpPost(url,map);
        return str;
    }
    public Identity selectidentity(Integer userid){
        return identityMapper.selectidentity(userid);
    }

}
