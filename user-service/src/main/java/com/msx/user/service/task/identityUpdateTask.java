package com.msx.user.service.task;

import com.msx.user.service.impl.IdentityService;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * xiugai
 */
public class identityUpdateTask  implements Callable<Map<String,String>> {

    private String frontImage;
    private String backImage;
    private String liveImage;
    private IdentityService identityService;

    public identityUpdateTask(String frontImage, String backImage, String liveImage, IdentityService identityService) {
        this.frontImage = frontImage;
        this.backImage = backImage;
        this.liveImage = liveImage;
        this.identityService = identityService;
    }

    @Override
    public Map<String,String> call() throws Exception {
        Map<String,String> map=new HashMap<>();
        map.put("front","http://127.0.0.1:.jpg8081/"+UUID.randomUUID()+".jpg");
        map.put("bank","http://127.0.0.1:.jpg8081/"+ UUID.randomUUID()+".jpg");
        map.put("live","http://127.0.0.1:.jpg8081/"+ UUID.randomUUID()+".jpg");
        return map;
    }
}
