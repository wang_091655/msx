package com.msx.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.msx.constants.IsDeleteEnum;
import com.msx.pojo.Operator;
import com.msx.pojo.User;
import com.msx.user.mapper.OperatorMapper;
import com.msx.user.mapper.UserMapper;
import com.msx.util.HttpClientUtils;
import com.msx.util.MD5Sign;
import com.msx.util.ResponseResult;
import com.msx.util.StatusCode;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运营商认证
 */
@Service
@Log4j2
public class OperatorService {

    @Value("${user.cardId}")
    private String CardId;
    @Value("${user.appSercret}")
    private String appSercret;

    @Autowired
    private OperatorMapper operatorMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    //根据用户id查询运营商表;
    public ResponseResult findOperatorByUserId(Integer userId){

        Example example = new Example(Operator.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId",userId);
        List<Operator> operators = operatorMapper.selectByExample(example);
        log.info(operators);

        if ( operators.size()==0){
            return new ResponseResult().error(StatusCode.ERROR,"抱歉没有该用户信息");
        }
        return new ResponseResult().success(operators.get(operators.size()-1));
    }


    public ResponseResult IdentityOperator(Operator operator){
            //调用第三方接口
            String s = this.IdentityOper(operator);
            //判断运营商信息
            JSONObject jsonObject = JSON.parseObject(s);
            log.info("查看调用接口运营商信息:"+jsonObject);

            if (String.valueOf(IsDeleteEnum.OK.getCode()).equals(jsonObject.getString("code"))){
                //添加信息
                operator.setCreateTime(new Date());
                operator.setMessage(jsonObject.toString());
                operator.setIsDelete(String.valueOf(IsDeleteEnum.OK.getCode()));
                operatorMapper.insertSelective(operator);

                //修改状态
                User user = new User();
                user.setId(operator.getUserId());
                user.setOperatorAuthentication(String.valueOf(IsDeleteEnum.ERROR.getCode()));
                userMapper.updateByPrimaryKeySelective(user);

                return new ResponseResult(StatusCode.OK,"运营商认证操作成功");
            }
        return new ResponseResult(StatusCode.LOGINERROR,"运营商认证异常,请重新认证!");

    }


    //运营商认证
    private String IdentityOper(Operator operator){
        String url="http://127.0.0.1:8080/api/common/mobileAuth";
        Map<String,String> map = new HashMap<>();
        map.put("merId",CardId);
        map.put("mobile",operator.getMobile());
        map.put("code",operator.getCode());
        map.put("sign", MD5Sign.sign(map,appSercret));
        String post = HttpClientUtils.httpPost(url, map);
        return post;
    }



}
