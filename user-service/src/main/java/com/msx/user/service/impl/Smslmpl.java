package com.msx.user.service.impl;

import com.msx.pojo.Sms;
import com.msx.user.mapper.SmsMapper;
import com.msx.util.HttpClientUtils;
import com.msx.util.MD5Sign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 发送信息
 * 保存记录
 */
@Service
public class Smslmpl {
    @Value("${user.cardId}")
    private String merId;
    @Value("${user.appSercret}")
    private String appSercret;

    @Autowired
    private SmsMapper smsMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送信息
     * @param sms
     * @return
     */
    public String sms(Sms sms){
        String url="http://127.0.0.1:8080/api/common/sendSms";
        Map<String,String> map = new HashMap<>();
        map.put("merId",merId);
        map.put("mobile",sms.getMobile());
        map.put("content",sms.getContent());
        map.put("sign", MD5Sign.sign(map,appSercret));
        String post = HttpClientUtils.httpPost(url, map);
        return post;
    }

    //添加
    public void SmsAdd(Sms sms){
        smsMapper.insertSelective(sms);
    }


}
