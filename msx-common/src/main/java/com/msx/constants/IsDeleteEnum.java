package com.msx.constants;

/**
 * @author dutao
 * @create: 2020-06-16 13:32
 */
public enum IsDeleteEnum {
    OK(0,"无效"),ERROR(1,"有效");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    IsDeleteEnum() {
    }

    IsDeleteEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
