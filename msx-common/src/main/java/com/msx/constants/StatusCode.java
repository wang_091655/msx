package com.msx.constants;

/**
 * @author dutao
 * @create: 2020-06-16 09:50
 */
public class StatusCode {
    public static final int OK=20000;//成功
    public static final int ERROR=20001;//失败
}
