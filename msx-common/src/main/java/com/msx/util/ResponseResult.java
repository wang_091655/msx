package com.msx.util;

import com.msx.constants.StatusCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dutao
 * @create: 2020-06-16 09:45
 */
@Data
public class ResponseResult implements Serializable {

    private Integer code;//返回码
    private String msg;
    private Object data;

    public ResponseResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseResult() {
    }

    public static  ResponseResult success(Object data){
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(StatusCode.OK);
        responseResult.setData(data);
        responseResult.setMsg("成功");
        return responseResult;
    }

    public static  ResponseResult success(){
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(StatusCode.OK);
        responseResult.setMsg("成功");
        return responseResult;
    }


    public static  ResponseResult error(){
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(StatusCode.ERROR);
        responseResult.setMsg("失败");
        return responseResult;
    }

    public static  ResponseResult error(Integer code,String msg){
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(code);
        responseResult.setMsg(msg);
        return responseResult;
    }

    public static  ResponseResult error(Object data){
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(StatusCode.ERROR);
        responseResult.setMsg("失败");
        responseResult.setData(data);
        return responseResult;
    }
}
